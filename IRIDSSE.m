%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Current(I) Real Imaginary Distribution System State Estimator (VRIDSSE)
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function   [Vmagn_status,Vph_status,Imagn_status,Iph_status,inj_status,P_post]=IRIDSSE...
    (Meas_vector_external,W,GridData,Test_SetUp)
% Jacobian matrix
[H] = Jacobian_m_IRIDSSE(GridData);
HW = H'*W;
G1 = HW*H;%gain matrix
G2 = G1;
P_post = inv(G2); %estimated state error covariance

if isfield(GridData, 'DM') == 1
    ref1 = 11*GridData.DM.NGF+10*GridData.DM.NGS+2*GridData.DM.Nload;
    if GridData.inj_status == 1
        P_post(:,ref1+1)=[];
        P_post(ref1+1,:)=[];

        for x = 1 : ref1 + 2*GridData.Lines_num
            P_post(:,x) = P_post(:,x) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)] );
        end
        for x = 1 : ref1 + 2*GridData.Lines_num
            P_post(x,:) = P_post(x,:) .* ([GridData.base_status; GridData.base_current*ones(2*GridData.Lines_num,1)] )';
        end
    end
end

%we initialize the state
if strcmp(GridData.type_of_model,'single_phase')==1
    Vmagn_status = ones(GridData.Nodes_num,1);
    Vph_status = zeros(GridData.Nodes_num,1);
    Imagn_status = zeros(GridData.Lines_num,1);
    Iph_status = zeros(GridData.Lines_num,1);
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    Vmagn_status=ones(3,GridData.Nodes_num);
    Vph_status=zeros(3,GridData.Nodes_num);
    Vph_status(2,:)=(4/3)*pi;
    Vph_status(3,:)=(2/3)*pi;
    Imagn_status = zeros(3,GridData.Lines_num);
    Iph_status = zeros(3,GridData.Lines_num);
end
if GridData.inj_status == 1 %in case we consider dynamic measurements
    inj_status =  zeros(11*GridData.DM.NGF+10*GridData.DM.NGS + 2*GridData.DM.Nload,1);
else
    inj_status  = 0;
end

max_delta=10; %dummy inizialitazion
iteration=1;

% Newton Rapson calculation of the state
while max_delta > 1e-12 &&  iteration < Test_SetUp.limit2
    iteration=iteration+1;
    [Meas_vector] = calc_Mvector(Vmagn_status,Vph_status,GridData,Meas_vector_external);% 1) Update vector of measurements
    [hx] = calc_hx_IRIDSSE(Vmagn_status,Vph_status,Imagn_status,Iph_status,GridData,inj_status);% 2) Calculate h(x) vector
    res = Meas_vector - hx;% build the residual vector
    HWres = HW*res;
    delta = G1\HWres;
    
    n = 1;
    if GridData.inj_status == 1 %in case we consider dynamic measuremetns
        inj_status = inj_status + delta(1:11*GridData.DM.NGF+10*GridData.DM.NGS + 2*GridData.DM.Nload,1) ;
        n = 11*GridData.DM.NGF+10*GridData.DM.NGS + 2*GridData.DM.Nload + 1;
    end
    
    Volts = Vmagn_status.*exp(sqrt(-1)*Vph_status);
    Amps =  Imagn_status.*exp(sqrt(-1)*Iph_status);
    %we update the status using delta
    if strcmp(GridData.type_of_model,'single_phase')==1
        
        if isfield(GridData,'rm_column') == 1
            if GridData.rm_column == 1
                Volts(1,1)=Volts(1,1)+delta(n);
            end
            Volts(1,1)=Volts(1,1)+delta(n);
            n=n+1;
            Volts(1,1)=Volts(1,1)+1i*delta(n);
        else
            Volts(1,1)=Volts(1,1)+delta(n);
        end
        Vmagn_status(1,1) = abs(Volts(1,1));
        Vph_status (1,1)  = angle(Volts(1,1));
        
        
        for i = 1:GridData.Lines_num
            n=n+1;
            Amps(i) = Amps(i) + delta(n,1);
            n=n+1;
            Amps(i,1) = Amps(i,1) + 1i * delta(n,1);
            Imagn_status(i,1) = abs(Amps(i,1));
            Iph_status (i,1)  = phase(Amps(i,1));
        end
        Vnode = zeros(GridData.Nodes_num,1);
        Vnode(1,1) = 1;
        while min(Vnode) < 1
            for b = 1 : GridData.Lines_num
                if Vnode(GridData.topology(2,b),1) == 1
                    if Vnode(GridData.topology(3,b),1) == 0
                        x = GridData.topology(3,b);
                        Vnode(x,1) = Vnode(x,1) + 1;
                        Volts(x,1) = Volts(GridData.topology(2,b),1) - ( GridData.R1(1,b)+1i*GridData.X1(1,b))*Amps(b,1);
                        Vmagn_status(x,1) =  abs(Volts(x,1));
                        Vph_status (x,1)  = (phase(Volts(x,1)));
                    end
                end
            end
        end
        
        
    elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
        Vreal(1,1)=real(Volts(1,1))+delta(1);
        Volts(1,1)=Vreal(1,1);
        Vreal(2,1)=real(Volts(2,1))+delta(2);
        Volts(2,1)=Vreal(2,1)+1i*Vreal(2,1)*3/sqrt(3);
        Vreal(3,1) = real(Volts(3,1))+delta(3);
        Volts(3,1) = Vreal(3,1)+1i*Vreal(3,1)*-3/sqrt(3);
        Vmagn_status (:,1) = abs(Volts(:,1));
        Vph_status   (:,1) = phase(Volts(:,1));
        Amps =  Imagn_status.*exp(sqrt(-1)*Iph_status);
        n = 3;
        for x = 1 : GridData.Lines_num
            for f = 1 : 3
                if GridData.present_line(f,x) > 0
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) +      delta(n);
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) + 1i * delta(n);
                    Imagn_status(f,x) = abs  (Amps(f,x));
                    Iph_status  (f,x) = phase(Amps(f,x));
                end
            end
        end
        Vnode = zeros(GridData.Nodes_num,1);
        Vnode(1,1) = 1;
        while min(Vnode) < 1
            for b = 1 : GridData.Lines_num
                if Vnode(GridData.topology(2,b),1) == 1
                    if Vnode(GridData.topology(3,b),1) == 0
                        x = GridData.topology(3,b);
                        Vnode(x,1) = Vnode(x,1) + 1;
                        Volts(:,x) = Volts(:,GridData.topology(2,b)) - ( GridData.R1(:,3*b-2:3*b)+1i*GridData.X1(:,3*b-2:3*b))*Amps(:,b);
                        Vmagn_status(:,x) =  abs(Volts(:,x));
                        Vph_status (:,x)  = (phase(Volts(:,x)));
                    end
                end
            end
        end
    end
    % We calculate the maximum delta so that we verify if we need to stop the Newton Rapson iterations
    max_delta=max(abs(delta));
end
Vph_status = wrapToPi(Vph_status);

if iteration == Test_SetUp.limit2 %in case we reach the maximum number of iterations
    max_delta
end
end