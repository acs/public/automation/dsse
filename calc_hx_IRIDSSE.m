%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code to calculate the h(x) vector for the IRIDSSE estimator. They are measurement matematically
% calculated based on the estimated state at the ith iteration
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [hx] = calc_hx_IRIDSSE(Vmagn_status,Vph_status,Imagn_status,Iph_status,GridData,inj_status)
if strcmp(GridData.type_of_model,'single_phase')==1
    hx = zeros(GridData.MeasNum,1);
    Volts = Vmagn_status(:).*exp(sqrt(-1)*Vph_status(:));
    n_inj_status = 0;
    % calculate h(x)
    for n=1:GridData.MeasNum
        if GridData.TypeMeas(n,1)==1 %real current
            for x = 1 : GridData.Lines_num
                if GridData.topology(2,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) - real(Imagn_status(x)*exp(sqrt(-1)*Iph_status(x))) ;
                elseif GridData.topology(3,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) + real(Imagn_status(x)*exp(sqrt(-1)*Iph_status(x))) ;
                end
            end
        end
        if GridData.TypeMeas(n,1)==2 %immaginary current
            for x = 1 : GridData.Lines_num
                if GridData.topology(2,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) - imag(Imagn_status(x)*exp(sqrt(-1)*Iph_status(x))) ;
                elseif GridData.topology(3,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) + imag(Imagn_status(x)*exp(sqrt(-1)*Iph_status(x))) ;
                end
            end
        end
        if GridData.TypeMeas(n,1)==3 %voltage magnitude - translated to Vreal
            hx(n,1) = real(Volts(GridData.LocationMeas(n,1)));
        end
        if GridData.TypeMeas(n,1)==4 %voltage phase angle - translated to Vimag
            hx(n,1) = imag(Volts(GridData.LocationMeas(n,1)));
        end
        if GridData.TypeMeas(n,1)==5 %current magnitude - translated to Ireal
            hx(n,1) = real(Imagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==6 %current phase - translated to Iimag
            hx(n,1) = imag(Imagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==7 %pflow  - translated to Ireal
            hx(n,1) = real(Imagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==8 %qflow -  - translated to Ireal
            hx(n,1) = imag(Imagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==100 %dynamic state
            n_inj_status = n_inj_status +1;
            hx(n,1) = inj_status(n_inj_status,1) ;
        end
    end
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    hx = zeros(GridData.MeasNum,1);
    Volts = Vmagn_status.*exp(sqrt(-1)*Vph_status);
    % calculate h(x)
    for n = 1 : GridData.MeasNum
        if GridData.TypeMeas(n,1) == 1 %real current
            for x = 1 : GridData.Lines_num
                if GridData.topology(2,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) - real(Imagn_status(GridData.PhaseMeas(n,1),x)*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),x))) ;
                elseif GridData.topology(3,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) + real(Imagn_status(GridData.PhaseMeas(n,1),x)*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),x))) ;
                end
            end
        end
        if GridData.TypeMeas(n,1)==2 %immaginary current
            for x = 1 : GridData.Lines_num
                if GridData.topology(2,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) - imag(Imagn_status(GridData.PhaseMeas(n,1),x)*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),x))) ;
                elseif GridData.topology(3,x) == GridData.LocationMeas(n,1)
                    hx(n,1) = hx(n,1) + imag(Imagn_status(GridData.PhaseMeas(n,1),x)*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),x))) ;
                end
            end
        end
        if GridData.TypeMeas(n,1)==3 %voltage magnitude - translated to Vreal
            hx(n,1) = real(Volts(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)));
        end
        if GridData.TypeMeas(n,1)==4 %voltage phase angle - translated to Vimag
            hx(n,1) = imag(Volts(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)));
        end
        if GridData.TypeMeas(n,1)==5 %current magnitude - translated to Ireal
            hx(n,1) = real(Imagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==6 %current phase - translated to Iimag
            hx(n,1) = imag(Imagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==7 %pflow  - translated to Ireal
            hx(n,1) = real(Imagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==8 %qflow -  - translated to Ireal
            hx(n,1) = imag(Imagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Iph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
    end
end
end
