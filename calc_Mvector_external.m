%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to generate the measurement vector from PowerData structure
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Meas_vector] = calc_Mvector_external(GridData,PowerData)
Meas_vector=zeros(GridData.MeasNum,1);
inj_status = 0;
if strcmp(GridData.type_of_model,'single_phase')==1
for n=1:GridData.MeasNum
    if GridData.TypeMeas(n,1)==1 %active power
        Meas_vector(n,1)= PowerData.Pinj(GridData.LocationMeas(n,1));
    end
    if GridData.TypeMeas(n,1)==2 %reactive power
        Meas_vector(n,1)= PowerData.Qinj(GridData.LocationMeas(n,1));
    end
    if GridData.TypeMeas(n,1)==3 %voltage magnitude - translated to Vreal
        Meas_vector(n,1)= + real(PowerData.Vmagn(GridData.LocationMeas(n,1))*exp(sqrt(-1)*PowerData.Vph(GridData.LocationMeas(n,1))));
    end
    if GridData.TypeMeas(n,1)==4 %voltage phase angle - translated to Vimag
        Meas_vector(n,1)= + imag(PowerData.Vmagn(GridData.LocationMeas(n,1))*exp(sqrt(-1)*PowerData.Vph(GridData.LocationMeas(n,1))));
    end
    if GridData.TypeMeas(n,1)==5 %current magnitude - translated to Ireal
        Meas_vector(n,1)= + real((PowerData.Imagn(GridData.LocationMeas(n,1)))*exp(sqrt(-1)*PowerData.Iph(GridData.LocationMeas(n,1))));
    end
    if GridData.TypeMeas(n,1)==6 %current phase - translated to Iimag
        Meas_vector(n,1)= + imag((PowerData.Imagn(GridData.LocationMeas(n,1)))*exp(sqrt(-1)*PowerData.Iph(GridData.LocationMeas(n,1))));
    end
    if GridData.TypeMeas(n,1)==7 %pflow  - translated to Ireal
        Meas_vector(n,1)= PowerData.Pflow(GridData.LocationMeas(n,1));
    end
    if GridData.TypeMeas(n,1)==8 %qflow -  - translated to Ireal
        Meas_vector(n,1)=PowerData.Qflow(GridData.LocationMeas(n,1));
    end
    if GridData.TypeMeas(n,1)==100 %injection states
        inj_status = inj_status +1;
        Meas_vector(n,1) = PowerData.x_status(inj_status,1) ;
    end
end
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    for n = 1 : GridData.MeasNum
        if GridData.TypeMeas(n,1)==1 %active power
            Meas_vector(n,1)= PowerData.Pinj(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1));
        end
        if GridData.TypeMeas(n,1)==2 %reactive power
            Meas_vector(n,1)= PowerData.Qinj(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1));
        end
        if GridData.TypeMeas(n,1)==3 %voltage magnitude - translated to Vreal
                Meas_vector(n,1)=+real(PowerData.Vmagn(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*PowerData.Vph(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==4 %voltage phase angle - translated to Vimag
                Meas_vector(n,1)=+imag(PowerData.Vmagn(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*PowerData.Vph(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==5 %current magnitude - translated to Ireal
                Meas_vector(n,1)=+real((PowerData.Imagn(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)))*exp(sqrt(-1)*PowerData.Iph(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==6 %current phase - translated to Iimag
                Meas_vector(n,1)=+imag((PowerData.Imagn(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)))*exp(sqrt(-1)*PowerData.Iph(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))));
        end
        if GridData.TypeMeas(n,1)==7 %pflow  - translated to Ireal
                Meas_vector(n,1)=PowerData.Pflow(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1));
        end
        if GridData.TypeMeas(n,1)==8 %qflow -  - translated to Ireal
                Meas_vector(n,1)=PowerData.Qflow(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1));
        end
    end
end
end
