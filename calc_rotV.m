%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate matrix to convert rectangular voltages into polar voltages
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ rotV,rm_column] = calc_rotV( GridData,PowerData,use_pu)

if isfield(GridData,'rm_column') == 1
    rm_column = GridData.rm_column;
else
    rm_column = 1;
end

if rm_column == 1
    rotV = zeros(2*GridData.Nodes_num-1,2*GridData.Nodes_num-1);
else
    rotV = zeros(2*GridData.Nodes_num,2*GridData.Nodes_num);
end

if use_pu == 0
    PowerDataVmagn = PowerData.Vmagn * GridData.base_voltage;
else
    PowerDataVmagn = PowerData.Vmagn ;
end

for x = 1 :  GridData.Nodes_num
    if rm_column == 1
        if x == 1
            rotV(1,1) =  cos(PowerData.Vph(1,1));
        else
        rotV(2*(x-1)+1-1,2*(x-1)+1-1) = cos(PowerData.Vph(x,1));
        rotV(2*(x-1)+1-1,2*(x-1)+2-1) = sin(PowerData.Vph(x,1));
        rotV(2*(x-1)+2-1,2*(x-1)+1-1) = - sin(PowerData.Vph(x,1))/PowerDataVmagn(x,1);
        rotV(2*(x-1)+2-1,2*(x-1)+2-1) =   cos(PowerData.Vph(x,1))/PowerDataVmagn(x,1);    
         end
    else
        rotV(2*(x-1)+1,2*(x-1)+1) = cos(PowerData.Vph(x,1));
        rotV(2*(x-1)+1,2*(x-1)+2) = sin(PowerData.Vph(x,1));
        rotV(2*(x-1)+2,2*(x-1)+1) = - sin(PowerData.Vph(x,1))/PowerDataVmagn(x,1);
        rotV(2*(x-1)+2,2*(x-1)+2) =   cos(PowerData.Vph(x,1))/PowerDataVmagn(x,1);
    end
end

end

