%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Update the measurement vector based on the estimated voltages
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Meas_vector] = calc_Mvector(Vmagn_status,Vph_status,GridData,Meas_vector_external)

Meas_vector = Meas_vector_external;
if strcmp(GridData.type_of_model,'single_phase')==1
for n=1:GridData.MeasNum
    if GridData.TypeMeas(n,1)==1 %real current
        for m = 1 : GridData.MeasNum
            if GridData.LocationMeas(m,1) == GridData.LocationMeas(n,1) && GridData.TypeMeas(m,1) == 2
                 S1 = (Meas_vector_external(n,1)+1i*Meas_vector_external(m,1));
            end
        end
        V1 = (Vmagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Vph_status(GridData.LocationMeas(n,1))));
        I_load1 =  conj(S1/V1);        
        Meas_vector(n,1)= + real(I_load1);
    end
    if GridData.TypeMeas(n,1)==2 %immaginary current
        for m = 1 : GridData.MeasNum
            if GridData.LocationMeas(m,1) == GridData.LocationMeas(n,1) && GridData.TypeMeas(m,1) == 1
                S1 = (Meas_vector_external(m,1)+1i*Meas_vector_external(n,1));
            end
        end
        Meas_vector(n,1)= - imag((1i*Meas_vector_external(n,1) + Meas_vector_external(n-1,1))/...
            (Vmagn_status(GridData.LocationMeas(n,1))*exp(sqrt(-1)*Vph_status(GridData.LocationMeas(n,1)))));
    end
end
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    for n = 1 : GridData.MeasNum
        if GridData.TypeMeas(n,1)==1 %real current
            for m = 1 : GridData.MeasNum
                if GridData.LocationMeas(m,1) == GridData.LocationMeas(n,1) && GridData.TypeMeas(m,1) == 2 && GridData.PhaseMeas(n,1) == GridData.PhaseMeas(m,1)
                    S1 = (Meas_vector_external(n,1)+1i*Meas_vector_external(m,1));
                end
            end
            V1 = Vmagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Vph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)));
            I_load1 =  conj(S1/V1);
            Meas_vector(n,1)= + real( I_load1 );
        end
        if GridData.TypeMeas(n,1)==2 %immaginary current
            for m = 1 : GridData.MeasNum
                if GridData.LocationMeas(m,1) == GridData.LocationMeas(n,1) && GridData.TypeMeas(m,1) == 1 && GridData.PhaseMeas(n,1) == GridData.PhaseMeas(m,1)
                    S1 = (Meas_vector_external(m,1)+1i*Meas_vector_external(n,1));
                end
            end
            V1 = Vmagn_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1))*exp(sqrt(-1)*Vph_status(GridData.PhaseMeas(n,1),GridData.LocationMeas(n,1)));
            I_load1 =  conj(S1/V1);
            Meas_vector(n,1)= imag( I_load1 );
        end
    end
end
end
