%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to merge together data coming from the MC simulations of the
% state estimator
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function[results_DSSE] = Data_Output(Vmagn_status,Vph_status,results_DSSE,GridData,PowerData,inj_status)

if strcmp(GridData.type_of_model,'single_phase')==1
    Imagn_status=zeros(1,GridData.Lines_num);
    Iph_status=zeros(1,GridData.Lines_num);
    Ireal_status=zeros(1,GridData.Lines_num);
    Iimag_status=zeros(1,GridData.Lines_num);
    err_Imagnitude=zeros(1,GridData.Lines_num);
    err_Iphase=zeros(1,GridData.Lines_num);
    err_Vmagnitude=zeros(1,GridData.Nodes_num);
    err_Vphase=zeros(1,GridData.Nodes_num);
    err_Ireal =zeros(1,GridData.Lines_num);
    err_Iimag =zeros(1,GridData.Lines_num);
    err_Vreal =zeros(1,GridData.Nodes_num);
    err_Vimag =zeros(1,GridData.Nodes_num);
    
    err_Vmagnitude(1,:) =     (PowerData.Vmagn - Vmagn_status);
    err_Vphase(1,:) =    wrapToPi(PowerData.Vph   - Vph_status);
    err_Vreal(1,:) =     (PowerData.Vmagn.*cos(PowerData.Vph) - Vmagn_status.*cos(Vph_status));%
    err_Vimag(1,:) =    (PowerData.Vmagn.*sin(PowerData.Vph) - Vmagn_status.*sin(Vph_status));%
    
    
    Volts = transpose(Vmagn_status.*exp(sqrt(-1)*Vph_status)); %voltage in rectangular format resulting from the power flow
    Vreal_status = real(Volts);
    Vimag_status = imag(Volts);
    % we apply some calculations to get also the error of current
    % magnitude and phase angle
    for m=1:GridData.Lines_num
        n_i = GridData.topology(2,m);
        n_f = GridData.topology(3,m);
        V_i = Volts(1,n_i);
        V_f = Volts(1,n_f);
        R   = GridData.R2(1,m);
        X   = GridData.X2(1,m);
        I_branch_f = + (R+1i*X)*(V_i-V_f);
        Imagn_status(1,m) = abs  (I_branch_f);
        Iph_status(1,m)   = (phase(I_branch_f));
        Ireal_status(1,m) = real(I_branch_f);
        Iimag_status(1,m) = imag(I_branch_f);
        err_Imagnitude(1,m)        = (PowerData.Imagn(m,1) - Imagn_status(1,m));
        err_Iphase(1,m)            = wrapToPi(PowerData.Iph  (m,1)  - Iph_status  (1,m));
        err_Ireal(1,m)             = PowerData.Imagn(m,1)*cos(PowerData.Iph(m,1)) - Ireal_status(1,m);
        err_Iimag(1,m)             = PowerData.Imagn(m,1)*sin(PowerData.Iph(m,1)) - Iimag_status(1,m);
    end
    
    results_DSSE.err_Vmagnitude        = [results_DSSE.err_Vmagnitude; err_Vmagnitude];
    results_DSSE.err_Vphase            = [results_DSSE.err_Vphase    ; err_Vphase];
    results_DSSE.err_Imagnitude        = [results_DSSE.err_Imagnitude; err_Imagnitude];
    results_DSSE.err_Iphase            = [results_DSSE.err_Iphase    ; err_Iphase];
    results_DSSE.err_Ireal             = [results_DSSE.err_Ireal; err_Ireal];
    results_DSSE.err_Iimag             = [results_DSSE.err_Iimag    ; err_Iimag];
    results_DSSE.err_Vreal             = [results_DSSE.err_Vreal; err_Vreal];
    results_DSSE.err_Vimag             = [results_DSSE.err_Vimag    ; err_Vimag];
    results_DSSE.Vmagn_status          = [results_DSSE.Vmagn_status; GridData.base_voltage * Vmagn_status'];
    results_DSSE.Vph_status            = [results_DSSE.Vph_status  ; Vph_status'];
    results_DSSE.Vreal_status          = [results_DSSE.Vreal_status; GridData.base_voltage * Vreal_status];
    results_DSSE.Vimag_status          = [results_DSSE.Vimag_status  ; GridData.base_voltage *Vimag_status];
    results_DSSE.Imagn_status          = [results_DSSE.Imagn_status; GridData.base_current * Imagn_status];
    results_DSSE.Iph_status            = [results_DSSE.Iph_status  ; Iph_status];
    results_DSSE.Ireal_status          = [results_DSSE.Ireal_status; GridData.base_current * Ireal_status];
    results_DSSE.Iimag_status          = [results_DSSE.Iimag_status; GridData.base_current * Iimag_status];
    
    results_DSSE.Vmagn_true            = [results_DSSE.Vmagn_true; GridData.base_voltage * PowerData.Vmagn'];
    results_DSSE.Vph_true              = [results_DSSE.Vph_true  ; wrapToPi(PowerData.Vph)'];
    results_DSSE.Vreal_true            = [results_DSSE.Vreal_true; GridData.base_voltage * (PowerData.Vmagn.*cos(PowerData.Vph))'];
    results_DSSE.Vimag_true            = [results_DSSE.Vimag_true; GridData.base_voltage * (PowerData.Vmagn.*sin(PowerData.Vph))'];
    results_DSSE.Imagn_true            = [results_DSSE.Imagn_true; GridData.base_current * PowerData.Imagn'];
    results_DSSE.Iph_true              = [results_DSSE.Iph_true  ;  wrapToPi(PowerData.Iph)'];
    results_DSSE.Ireal_true            = [results_DSSE.Ireal_true; GridData.base_current * (PowerData.Imagn.*cos(PowerData.Iph))'];
    results_DSSE.Iimag_true            = [results_DSSE.Iimag_true; GridData.base_current * (PowerData.Imagn.*sin(PowerData.Iph))'];
    
    if GridData.inj_status == 1
        err_inj_status(1,:) = (PowerData.x_status - inj_status)';
        results_DSSE.err_inj_status  = [results_DSSE.err_inj_status; err_inj_status];
    end
    
elseif  strcmp(GridData.type_of_model,'three_phase_sequence')==1 || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    Imagn_status=zeros(3,GridData.Lines_num);
    Iph_status=zeros(3,GridData.Lines_num);
    Ireal_status=zeros(3,GridData.Lines_num);
    Iimag_status=zeros(3,GridData.Lines_num);
    err_Imagnitude=zeros(3,GridData.Lines_num);
    err_Iphase=zeros(3,GridData.Lines_num);
    err_Vmagnitude=zeros(3,GridData.Nodes_num);
    err_Vphase=zeros(3,GridData.Nodes_num);
    err_Ireal =zeros(3,GridData.Lines_num);
    err_Iimag =zeros(3,GridData.Lines_num);
    err_Vreal =zeros(3,GridData.Nodes_num);
    err_Vimag =zeros(3,GridData.Nodes_num);
    
    err_Vmagnitude =     (PowerData.Vmagn - Vmagn_status);
    err_Vphase =    wrapToPi(PowerData.Vph   - Vph_status);
    err_Vreal =     (PowerData.Vmagn.*cos(PowerData.Vph) - Vmagn_status.*cos(Vph_status));%
    err_Vimag =    (PowerData.Vmagn.*sin(PowerData.Vph) - Vmagn_status.*sin(Vph_status));% 
    Volts = transpose(Vmagn_status.*exp(sqrt(-1)*Vph_status)); %voltage in rectangular format resulting from the power flow
    Vreal_status = real(Volts);
    Vimag_status = imag(Volts);
    
    % we apply some calculations to get also the error of current
    % magnitude and phase angle
    for m = 1:GridData.Lines_num
        n_i = GridData.topology(2,m);
        n_f = GridData.topology(3,m);
        V_i = transpose(Volts(n_i,:));
        V_f = transpose(Volts(n_f,:));
        R   = GridData.R2(:,3*(m-1)+1:3*m);
        X   = GridData.X2(:,3*(m-1)+1:3*m);
        I_branch_f = + (R+1i*X)*(V_i-V_f);
        Imagn_status(:,m) = abs  (I_branch_f);
        Iph_status(:,m)   = (phase(I_branch_f));
        Ireal_status(:,m) = real(I_branch_f);
        Iimag_status(:,m) = imag(I_branch_f);
        err_Imagnitude(:,m)        = (PowerData.Imagn(:,m) - Imagn_status(:,m));
        err_Iphase(:,m)            = wrapToPi(PowerData.Iph(:,m)  - Iph_status  (:,m));
        err_Ireal(:,m)             = PowerData.Imagn(:,m).*cos(PowerData.Iph(:,m)) - Ireal_status(:,m);
        err_Iimag(:,m)             = PowerData.Imagn(:,m).*sin(PowerData.Iph(:,m)) - Iimag_status(:,m);
    end
    
    results_DSSE.err_Vmagnitude        = [results_DSSE.err_Vmagnitude; err_Vmagnitude];
    results_DSSE.err_Vphase            = [results_DSSE.err_Vphase    ; err_Vphase];
    results_DSSE.err_Imagnitude        = [results_DSSE.err_Imagnitude; err_Imagnitude];
    results_DSSE.err_Iphase            = [results_DSSE.err_Iphase    ; err_Iphase];
    results_DSSE.err_Ireal             = [results_DSSE.err_Ireal; err_Ireal];
    results_DSSE.err_Iimag             = [results_DSSE.err_Iimag    ; err_Iimag];
    results_DSSE.err_Vreal             = [results_DSSE.err_Vreal; err_Vreal];
    results_DSSE.err_Vimag             = [results_DSSE.err_Vimag    ; err_Vimag];
    results_DSSE.Vmagn_status          = [results_DSSE.Vmagn_status; GridData.base_voltage * Vmagn_status'];
    results_DSSE.Vph_status            = [results_DSSE.Vph_status  ; Vph_status'];
    results_DSSE.Vreal_status                 = [results_DSSE.Vreal_status; GridData.base_voltage * Vreal_status];
    results_DSSE.Vimag_status                 = [results_DSSE.Vimag_status  ; GridData.base_voltage *Vimag_status];
    results_DSSE.Imagn_status          = [results_DSSE.Imagn_status; GridData.base_current * Imagn_status];
    results_DSSE.Iph_status            = [results_DSSE.Iph_status  ; Iph_status];
    results_DSSE.Ireal_status                 = [results_DSSE.Ireal_status; GridData.base_current * Ireal_status];
    results_DSSE.Iimag_status                 = [results_DSSE.Iimag_status; GridData.base_current * Iimag_status];
    
    results_DSSE.Vmagn_true          = [results_DSSE.Vmagn_true; GridData.base_voltage * PowerData.Vmagn'];
    results_DSSE.Vph_true            = [results_DSSE.Vph_true  ; PowerData.Vph'];
    results_DSSE.Vreal_true                 = [results_DSSE.Vreal_true; GridData.base_voltage * (PowerData.Vmagn.*cos(PowerData.Vph))'];
    results_DSSE.Vimag_true                 = [results_DSSE.Vimag_true; GridData.base_voltage * (PowerData.Vmagn.*sin(PowerData.Vph))'];
    results_DSSE.Imagn_true          = [results_DSSE.Imagn_true; GridData.base_current * PowerData.Imagn'];
    results_DSSE.Iph_true            = [results_DSSE.Iph_true  ; PowerData.Iph'];
    results_DSSE.Ireal_true                 = [results_DSSE.Ireal_true; GridData.base_current * (PowerData.Imagn.*cos(PowerData.Iph))'];
    results_DSSE.Iimag_true                 = [results_DSSE.Iimag_true; GridData.base_current * (PowerData.Imagn.*sin(PowerData.Iph))'];
    
end


end