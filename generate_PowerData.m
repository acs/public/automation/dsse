%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code to generate power flow data of the grid for state estimation
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [PowerData]=generate_PowerData(GridData,name_of_model)

if strcmp(GridData.type_of_model,'single_phase') == 1
    
    %*******************************************************************
    % sample grid power : subsitute with your grid power data    
    %run sample_singlephase1
    %run sample_singlephase2
    %run CIGRE_MV
    eval(name_of_model);
    %*******************************************************************
    
    P_tot = (P_load-P_gen)/(GridData.base_power);
    Q_tot = (Q_load-Q_gen)/(GridData.base_power);

    PowerDataIn = struct;
    PowerDataIn.Pinj = [-sum(P_tot),P_tot];
    PowerDataIn.Qinj = [-sum(Q_tot),Q_tot];
    PowerDataIn.Vmagn = V_magn;
    PowerDataIn.Vph = V_ph;
    
    % based on the power at the PQ nodes and the voltage at the slack bus
    % the reference power flow profile is generated in the following code
    
    [~,Combination_devices,Accuracy] = generate_PFConfData(GridData);
    [W,GridData] = Weight_m(GridData,PowerDataIn,Combination_devices,Accuracy);
    
    Vmagn_status=ones(GridData.Nodes_num,1);
    Vph_status=zeros(GridData.Nodes_num,1);
    Imagn_status=ones(GridData.Lines_num,1);
    Iph_status=zeros(GridData.Lines_num,1);
    [H] = Jacobian_m_IRIDSSE(GridData);
    HW = H'*W;
    G1 = HW*H; 
    limit1 = 1e-5;
    limit2 = 50;
    max_delta = zeros(limit2,1);
    max_delta(1) = 10;
    iteration = 1;
    [Meas_vector_external] = calc_Mvector_external(GridData,PowerDataIn);
    % Newton Rapson algorithm 
    while max_delta(iteration,1) > limit1 &&  iteration < limit2
        [Meas_vector] = calc_Mvector(Vmagn_status,Vph_status,GridData,Meas_vector_external);
        [hx] = calc_hx_IRIDSSE(Vmagn_status,Vph_status,Imagn_status,Iph_status,GridData);
        res = Meas_vector - hx;
        HWres = HW*res;
        delta = G1\HWres;
        n = 1;
        Volts = Vmagn_status.*exp(sqrt(-1)*Vph_status);
        Amps =  Imagn_status.*exp(sqrt(-1)*Iph_status);
            Volts(1,1) = Volts(1,1)+delta(n,1);
            Vmagn_status(1,1) = Volts(1,1);
            Vph_status (1,1) = 0;
            for x = 1:GridData.Lines_num
                n=n+1;
                Amps(x) = Amps(x) + delta(n,1);
                n=n+1;
                Amps(x,1) = Amps(x,1) + 1i * delta(n,1);
                Imagn_status(x,1) = abs(Amps(x,1));
                Iph_status (x,1)  = phase(Amps(x,1));
                
                in1 = GridData.topology(2,x);
                fin1 = GridData.topology(3,x);
                Volts(fin1,1) = Volts(in1,1) - ( GridData.R1(1,x)+1i*GridData.X1(1,x))*Amps(x,1);
                Vmagn_status(fin1,1) = abs(Volts(fin1,1));
                Vph_status (fin1,1) = (phase(Volts(fin1,1)));
            end %the state is updated
            Vnode = zeros(GridData.Nodes_num,1);
            Vnode(1,1) = 1;
            while min(Vnode) < 1
                for b = 1 : GridData.Lines_num
                    if Vnode(GridData.topology(2,b),1) == 1
                        if Vnode(GridData.topology(3,b),1) == 0
                            x = GridData.topology(3,b);
                            Vnode(x,1) = Vnode(x,1) + 1;
                            Volts(x,1) = Volts(GridData.topology(2,b),1) - ( GridData.R1(1,b)+1i*GridData.X1(1,b))*Amps(b,1);
                            Vmagn_status(x,1) =  abs(Volts(x,1));
                            Vph_status(x,1)  = (phase(Volts(x,1)));
                        end
                    end
                    
                    
                    if Vnode(GridData.topology(3,b),1) == 1
                        if Vnode(GridData.topology(2,b),1) == 0
                            x = GridData.topology(2,b);
                            Vnode(x,1) = Vnode(x,1) + 1;
                            Volts(x,1) = Volts(GridData.topology(3,b),1) - ( GridData.R1(1,b)+1i*GridData.X1(1,b))*Amps(b,1);
                            Vmagn_status(x,1) =  abs(Volts(x,1));
                            Vph_status(x,1)  = (phase(Volts(x,1)));
                        end
                    end
                    
                                        
                end
            end
        iteration=iteration+1;
        max_delta(iteration,1)=max(abs(delta));
    end
    Vph_status = wrapToPi(Vph_status);
    I_load=zeros(GridData.Nodes_num,1);

    for x=1:GridData.Nodes_num
        for m=1:GridData.Lines_num
            if GridData.topology(2,m)== x
                I_load(x) = I_load(x) - Amps(GridData.topology(1,m));
            elseif GridData.topology(3,m)== x
                I_load(x) = I_load(x) + Amps(GridData.topology(1,m));
            end
        end
    end
    
    % once the solution of the PF is found, the struct PowerData is
    % populated with the data that later will be given to the state
    % estimator
    
    PowerData=struct;
    for m=1:GridData.Lines_num %the assumption is that all power flows are calculated at the end of the lines
        PowerData.Pflow(m,1)=real(Volts(GridData.topology(3,m))*Amps(m)');
        PowerData.Qflow(m,1)=imag(Volts(GridData.topology(3,m))*Amps(m)');
    end
    % we can calculate the power flows
    for x=1:GridData.Nodes_num
        PowerData.Pinj(x,1)=real(Volts(x)*I_load(x)');
        PowerData.Qinj(x,1)=imag(Volts(x)*I_load(x)');
    end
    PowerData.Vmagn=Vmagn_status;
    PowerData.Vph=Vph_status;
    PowerData.Imagn=Imagn_status;
    PowerData.Iph=Iph_status;
    
elseif strcmp(GridData.type_of_model,'three_phase_sequence') == 1
    
    %*******************************************************************
    % sample grid power : subsitute with your grid power data    
    %run sample_threephase1
    eval(name_of_model);
    %*******************************************************************
    
    P_tot = (P_load-P_gen)/(GridData.base_power);
    Q_tot = (Q_load-Q_gen)/(GridData.base_power);
    
    PowerDataIn=struct;
    PowerDataIn.Vmagn = V_magn;
    for f = 1 : 3
        PowerDataIn.Pinj(f,:) = [-sum(P_tot(f,:)),P_tot(f,:)];
        PowerDataIn.Qinj(f,:) = [-sum(Q_tot(f,:)),Q_tot(f,:)];
        if f == 1
            PowerDataIn.Vph(f,:) = V_ph(f,:);
        elseif f == 2
            PowerDataIn.Vph(f,:) = V_ph(f,:) + (4/3)*pi;
        elseif f == 3
            PowerDataIn.Vph(f,:) = V_ph(f,:) + (2/3)*pi;
        end
    end
    
    [~,Combination_devices,Accuracy] = generate_PFConfData(GridData);
    [W,GridData] = Weight_m(GridData,PowerDataIn,Combination_devices,Accuracy);
    Vmagn_status=ones(3,GridData.Nodes_num);
    Vph_status=zeros(3,GridData.Nodes_num);
    Vph_status(2,:)=(4/3)*pi;
    Vph_status(3,:)=(2/3)*pi;
    Imagn_status=zeros(3,GridData.Lines_num);
    Iph_status=zeros(3,GridData.Lines_num);
    Iph_status(2,:)=(4/3)*pi;
    Iph_status(3,:)=(2/3)*pi;
    [H] = Jacobian_m_IRIDSSE(GridData);
    HW = H'*W;
    G1 = HW*H;
    limit1 = 1e-5;
    limit2 = 50;
    max_delta = zeros(limit2,1);
    max_delta(1) = 10;
    iteration = 1;
    [Meas_vector_external] = calc_Mvector_external(GridData,PowerDataIn);
     % Newton Rapson algorithm 
    while max_delta(iteration,1) > limit1 &&  iteration < limit2
        iteration=iteration+1;
        [Meas_vector] = calc_Mvector(Vmagn_status,Vph_status,GridData,Meas_vector_external);
        [hx] = calc_hx_IRIDSSE(Vmagn_status,Vph_status,Imagn_status,Iph_status,GridData);
        res = Meas_vector-hx;
        HWres = HW*res;
        delta = G1\HWres;
        Volts = Vmagn_status .* exp(sqrt(-1)*Vph_status);
        Vreal(1,1)=real(Volts(1,1))+delta(1);
        Volts(1,1)=Vreal(1,1);
        Vreal(2,1)=real(Volts(2,1))+delta(2);
        Volts(2,1)=Vreal(2,1)+1i*Vreal(2,1)*3/sqrt(3);
        Vreal(3,1) = real(Volts(3,1))+delta(3);
        Volts(3,1) = Vreal(3,1)+1i*Vreal(3,1)*-3/sqrt(3);
        Vmagn_status (:,1) = abs(Volts(:,1));
        Vph_status   (:,1) = phase(Volts(:,1));
        Amps =  Imagn_status.*exp(sqrt(-1)*Iph_status);
        n = 3;
        for x = 1 : GridData.Lines_num
            for f = 1 : 3
                if GridData.present_line(f,x) > 0
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) + delta(n);
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) + 1i*delta(n);
                    Imagn_status(f,x) = abs(Amps(f,x));
                    Iph_status (f,x)  = phase(Amps(f,x));
                end
            end
        end
        Vnode = zeros(GridData.Nodes_num,1);
        Vnode(1,1) = 1;
        while min(Vnode) < 1
            for b = 1 : GridData.Lines_num
                if Vnode(GridData.topology(2,b),1) == 1
                    if Vnode(GridData.topology(3,b),1) == 0
                        x = GridData.topology(3,b);
                        Vnode(x,1) = Vnode(x,1) + 1;
                        Volts(:,x) = Volts(:,GridData.topology(2,b)) - ( GridData.R1(:,3*b-2:3*b)+1i*GridData.X1(:,3*b-2:3*b))*Amps(:,b);
                        Vmagn_status(:,x) =  abs(Volts(:,x));
                        Vph_status (:,x)  = (phase(Volts(:,x)));
                    end
                end
            end
        end
        
        max_delta(iteration,1) = max(abs(delta));
        
        I_load=zeros(3,GridData.Nodes_num);
        for x = 1 : GridData.Nodes_num
            for m = 1 : GridData.Lines_num
                if GridData.topology(2,m)== x
                    I_load(:,x) = I_load(:,x) - Amps(:,m);
                elseif GridData.topology(3,m)== x
                    I_load(:,x) = I_load(:,x) + Amps(:,m);
                end
            end
        end
        
    end
    
    % once the solution of the PF is found, the struct PowerData is
    % populated with the data that later will be given to the state
    % estimator
    PowerData=struct;
    for m=1:GridData.Lines_num %the assumption is that all power flows are calculated at the end of the lines
        PowerData.Pflow(:,m)=real(Volts(:,GridData.topology(3,m)).*conj(Amps(:,m)));
        PowerData.Qflow(:,m)=imag(Volts(:,GridData.topology(3,m)).*conj(Amps(:,m)));
    end
    % we can calculate the power flows
    for x=1:GridData.Nodes_num
        PowerData.Pinj(:,x)=real(Volts(:,x).*conj(I_load(:,x)));
        PowerData.Qinj(:,x)=imag(Volts(:,x).*conj(I_load(:,x)));
        PowerData.Vmagn(:,x)=abs(Volts(:,x));
        PowerData.Vph(:,x)=phase(Volts(:,x));
    end
    PowerData.Vph = wrapToPi(PowerData.Vph);
    PowerData.Imagn=abs(Amps);
    PowerData.Iph(1,:)=wrapToPi(phase(Amps(1,:)));
    PowerData.Iph(2,:)=wrapToPi(phase(Amps(2,:)));
    PowerData.Iph(3,:)=wrapToPi(phase(Amps(3,:)));
    
elseif  strcmp(GridData.type_of_model,'three_phase_unbalance') == 1
    
    %*******************************************************************
    % sample grid power : subsitute with your grid power data
    %     V_magn = ones(3,GridData.Nodes_num);
    %     V_ph   = zeros(3,GridData.Nodes_num);
    %     A_load_a = 1000*[50,  0,  0, 20, 20,20]; %load consumption in VA
    %     A_load_b = 1000*[30, 30,  0, 20, 25, 0]; %load consumption in VA
    %     A_load_c = 1000*[40, 30, 50, 13, 20, 0]; %load consumption in VA
    %     PF = [0.97,0.90,0.97,0.97,0.97,0.97];
    %     P_load = [A_load_a.*PF;A_load_b.*PF;A_load_c.*PF];
    %     Q_load = [A_load_a.*sin(acos(PF));A_load_b.*sin(acos(PF));A_load_c.*sin(acos(PF))];
    %     P_gen  = zeros(size(P_load));
    %     Q_gen  = zeros(size(P_load));
    %*******************************************************************
    
    %*******************************************************************
    % sample grid power : IEEE 123
    V_magn = ones(3,GridData.Nodes_num);
    V_ph   = zeros(3,GridData.Nodes_num);
    A_load_a=transpose(1000*[40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;20+10i;0+0i;40+20i;20+10i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;40+20i;0+0i;0+0i;0+0i;40+20i;0+0i;40+20i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;20+10i;0+0i;0+0i;20+10i;20+10i;35+25i;70+50i;35+25i;0+0i;20+10i;40+20i;40+20i;0+0i;20+10i;0+0i;0+0i;0+0i;0+0i;20+10i;0+0i;0+0i;40+20i;0+0i;35+25i;0+0i;0+0i;20+10i;40+20i;20+10i;40+20i;0+0i;0+0i;0+0i;0+0i;105+80i;0+0i;0+0i;40+20i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;20+10i;20+10i;40+20i;20+10i;0+0i;0+0i;0+0i;0+0i]);
    A_load_b=transpose(1000*[0+0i; 20+10i;0+0i;0+0i;0+0i;   0+0i;0+0i;   0+0i;0+0i;0+0i;0+0i;20+10i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i  ;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;       20+10i;20+10i;0+0i;0+0i;0+0i;20+10i;0+0i;0+0i;0+0i;  35+25i;70+50i;70+50i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;      20+10i;0+0i;20+10i;20+10i;0+0i;0+0i;0+0i;0+0i;75+35i;35+25i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;     70+50i;40+20i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;20+10i;40+20i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;20+10i;20+10i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i ]);
    A_load_c=transpose(1000*[0+0i;0+0i;0+0i;40+20i;20+10i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;   40+20i;20+10i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;  40+20i;20+10i;20+10i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;20+10i;0+0i;0+0i;0+0i;0+0i;0+0i;35+25i;70+50i;35+20i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;70+50i;75+35i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;40+20i;40+20i;70+50i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;20+10i;20+10i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;40+20i;0+0i;20+10i;40+20i;40+20i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i;0+0i]);
%    limit = 5-1;
%     A_load_a = A_load_a(1:limit );
%     A_load_b = A_load_b(1:limit );
%     A_load_c = A_load_c(1:limit );
    P_load = [real(A_load_a);real(A_load_b);real(A_load_c)];
    Q_load = [imag(A_load_a);imag(A_load_b);imag(A_load_c)];
    P_gen  = zeros(size(P_load));
    Q_gen  = zeros(size(P_load));
    %*******************************************************************
    
    P_tot = (P_load - P_gen)/GridData.base_power;
    Q_tot = (Q_load - Q_gen)/GridData.base_power;
    
    PowerDataIn=struct;
    PowerDataIn.Vmagn = V_magn;
    for f = 1 : 3
        PowerDataIn.Pinj(f,:) = [-sum(P_tot(f,:)),P_tot(f,:)];
        PowerDataIn.Qinj(f,:) = [-sum(Q_tot(f,:)),Q_tot(f,:)];
        if f == 1
            PowerDataIn.Vph(f,:) = V_ph(f,:);
        elseif f == 2
            PowerDataIn.Vph(f,:) = V_ph(f,:) + (4/3)*pi;
        elseif f == 3
            PowerDataIn.Vph(f,:) = V_ph(f,:) + (2/3)*pi;
        end
    end
    
    [~,Combination_devices,Accuracy] = generate_PFConfData(GridData);
    [W,GridData] = Weight_m(GridData,PowerDataIn,Combination_devices,Accuracy);
    
    Vmagn_status=ones(3,GridData.Nodes_num);
    Vph_status=zeros(3,GridData.Nodes_num);
    Vph_status(2,:)=(4/3)*pi;
    Vph_status(3,:)=(2/3)*pi;
    Imagn_status=zeros(3,GridData.Lines_num);
    Iph_status=zeros(3,GridData.Lines_num);
    %     Iph_status(2,:)=(4/3)*pi;
    %     Iph_status(3,:)=(2/3)*pi;
    [H] = Jacobian_m_IRIDSSE(GridData);
    HW = H'*W;
    G1 = HW*H;
    limit1 = 1e-5;
    limit2 = 50;
    max_delta = zeros(limit2,1);
    max_delta(1) = 10;
    iteration = 1;
    [Meas_vector_external] = calc_Mvector_external(GridData,PowerDataIn);
    % Newton Rapson algorithm 
    while max_delta(iteration,1) > limit1 &&  iteration < limit2
        iteration=iteration+1;
        [Meas_vector] = calc_Mvector(Vmagn_status,Vph_status,GridData,Meas_vector_external);
        [hx] = calc_hx_IRIDSSE(Vmagn_status,Vph_status,Imagn_status,Iph_status,GridData);
        res = Meas_vector-hx;
        HWres = HW*res;
        delta = G1\HWres;
        Volts = Vmagn_status .* exp(sqrt(-1)*Vph_status);
        Vreal(1,1)= real(Volts(1,1))+delta(1);
        Volts(1,1)= Vreal(1,1);
        Vreal(2,1)= real(Volts(2,1))+delta(2);
        Volts(2,1)= Vreal(2,1)+1i*Vreal(2,1)*3/sqrt(3);
        Vreal(3,1) = real(Volts(3,1))+delta(3);
        Volts(3,1) = Vreal(3,1)+1i*Vreal(3,1)*-3/sqrt(3);
        Vmagn_status (:,1) = abs(Volts(:,1));
        Vph_status   (:,1) = phase(Volts(:,1));
        Amps =  Imagn_status.*exp(sqrt(-1)*Iph_status);
        n = 3;
        for x = 1 : GridData.Lines_num
            for f = 1 : 3
                if GridData.present_line(f,x) > 0
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) + delta(n);
                    n = n + 1;
                    Amps(f,x)         = Amps(f,x) + 1i*delta(n);
                    Imagn_status(f,x) = abs(Amps(f,x));
                    Iph_status (f,x)  = phase(Amps(f,x));
                end
            end
        end
        Vnode = zeros(GridData.Nodes_num,1);
        Vnode(1,1) = 1;
        while min(Vnode) < 1
            for b = 1 : GridData.Lines_num
                if Vnode(GridData.topology(2,b),1) == 1
                    if Vnode(GridData.topology(3,b),1) == 0
                        x = GridData.topology(3,b);
                        Vnode(x,1) = Vnode(x,1) + 1;
                        Volts(:,x) = Volts(:,GridData.topology(2,b)) - ( GridData.R1(:,3*b-2:3*b)+1i*GridData.X1(:,3*b-2:3*b))*Amps(:,b);
                        Vmagn_status(:,x) =  abs(Volts(:,x));
                        Vph_status (:,x)  = (phase(Volts(:,x)));
                    end
                end
            end
        end
        
        max_delta(iteration,1) = max(abs(delta));
        I_load=zeros(3,GridData.Nodes_num);
        for x = 1 : GridData.Nodes_num
            for m = 1 : GridData.Lines_num
                if GridData.topology(2,m)== x
                    I_load(:,x) = I_load(:,x) - Amps(:,m);
                elseif GridData.topology(3,m)== x
                    I_load(:,x) = I_load(:,x) + Amps(:,m);
                end
            end
        end 
    end


% once the solution of the PF is found, the struct PowerData is
% populated with the data that later will be given to the state
% estimator
PowerData=struct;
for m=1:GridData.Lines_num %the assumption is that all power flows are calculated at the end of the lines
    PowerData.Pflow(:,m)=real(Volts(:,GridData.topology(3,m)).*conj(Amps(:,m)));
    PowerData.Qflow(:,m)=imag(Volts(:,GridData.topology(3,m)).*conj(Amps(:,m)));
end
% we can calculate the power flows
for x=1:GridData.Nodes_num
    PowerData.Pinj(:,x)=real(Volts(:,x).*conj(I_load(:,x)));
    PowerData.Qinj(:,x)=imag(Volts(:,x).*conj(I_load(:,x)));
    PowerData.Vmagn(:,x)=abs(Volts(:,x));
    PowerData.Vph(:,x)=phase(Volts(:,x));
end
PowerData.Vph = wrapToPi(PowerData.Vph);
PowerData.Imagn=abs(Amps);
PowerData.Iph(1,:)=wrapToPi(phase(Amps(1,:)));
PowerData.Iph(2,:)=wrapToPi(phase(Amps(2,:)));
PowerData.Iph(3,:)=wrapToPi(phase(Amps(3,:)));
end
end
