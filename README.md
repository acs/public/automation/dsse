# Distribution System State Estimator (DSSE)

This repository contains the following material:
- Matlab code for rectangular current distribution state estimation
- Matlab code for rectangular voltage distribution state estimation
- Matlab code to collect error metrics and calculate uncertainty of estimators

The code runs in Matlab environment

## Instructions to test the state estimator

- Run in Matlab the file test_steady_DSSE.m (consider that the number of MC is 10000, it will be quite time consuming, you can edit it at line 28 of generate_DSSEConfData.m)

## Copyright

2018, Institute for Automation of Complex Power Systems, EONERC  

## License

This project is released under the terms of the [GPL version 3](LICENSE).

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

For other licensing options please consult [Prof. Antonello Monti](mailto:amonti@eonerc.rwth-aachen.de).

## Contact

[![EONERC ACS Logo](https://git.rwth-aachen.de/acs/public/villas/VILLASnode/raw/develop/doc/pictures/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Andrea Angioni <aangioni@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de) 




