%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Code to generate sample data
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
base_power = 1e5; %Watts
base_voltage = 400/sqrt(3); %Volts
Nodes =            [1,2,3,4,5,6]; %nodes names
topology_initial =      [1    , 2     ,3   ,4    ,5  ]; %initial nodes of each line
topology_final   =      [2    , 3     ,4   ,5    ,6  ]; %final nodes of each line
topology  = [topology_initial;topology_final]; % lines intial and final nodes
Nodes_num = length(Nodes); %number of nodes in the grid
Lines_num = length(topology_initial); %number of lines in the grid
topology  = [(1:Lines_num);topology]; % the topology matrix contains also a row with the names of the lines
base_Z    = (base_voltage^2)/base_power; %base impedance
length_cable =     [0.000207  ,0.000489 ,0.000489,0.000489 ,0.000489]; %length in km of each line
% Pi model of the lines
R1 = length_cable.*     [0.152,0.152,0.152,0.152,0.152]; %ohm  per km, multiplied by length of the lines
X1 = length_cable.*     [0.113,0.113,0.113,0.113,0.113]; %ohm per km, multiplied by length of the lines
B1 = zeros(size(topology_initial));
G1 = zeros(size(topology_initial));


V_magn = ones(1,length(Nodes));
V_ph   = zeros(1,length(Nodes));
A_load = 1000*[200 , 200, 20 , 20,  20,   20];
PF          = [0.97,0.97,0.97,0.97,0.97,0.97];
%     A_load = [2500,2500];
%     PF          = [0.95,0.95];
P_load = A_load.*PF;
Q_load = A_load.*sin(acos(PF));
P_gen  = zeros(size(P_load));
Q_gen  = zeros(size(P_load));