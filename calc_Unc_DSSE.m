%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the uncertainty of the state estimator in the MC
% simulation
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function[results_DSSE_Uncertainty] = calc_Unc_DSSE( results_DSSE,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp)

if strcmp(GridData.type_of_model,'single_phase')==1
    use_pu = 1;
    results_DSSE_Uncertainty=struct;
    std_err_Vmagnitude_emp =zeros(GridData.Nodes_num,1);
    std_err_Vphase_emp     =zeros(GridData.Nodes_num,1);
    std_err_Vreal_emp      =zeros(GridData.Nodes_num,1);
    std_err_Vimag_emp      =zeros(GridData.Nodes_num,1);
    mean_err_Vmagnitude_emp=zeros(GridData.Nodes_num,1);
    mean_err_Vphase_emp    =zeros(GridData.Nodes_num,1);
    rms_err_Vmagnitude_emp =zeros(GridData.Nodes_num,1);
    rms_err_Vphase_emp     =zeros(GridData.Nodes_num,1);
    rms_err_Vreal_emp      =zeros(GridData.Nodes_num,1);
    rms_err_Vimag_emp      =zeros(GridData.Nodes_num,1);
    
    
    std_err_Imagnitude_emp =zeros(GridData.Lines_num,1);
    std_err_Iphase_emp     =zeros(GridData.Lines_num,1);
    std_err_Ireal_emp      =zeros(GridData.Lines_num,1);
    std_err_Iimag_emp      =zeros(GridData.Lines_num,1);
    mean_err_Imagnitude_emp=zeros(GridData.Lines_num,1);
    mean_err_Iphase_emp    =zeros(GridData.Lines_num,1);
    mean_err_Ireal_emp     =zeros(GridData.Lines_num,1);
    mean_err_Iimag_emp     =zeros(GridData.Lines_num,1);
    rms_err_Imagnitude_emp =zeros(GridData.Lines_num,1);
    rms_err_Iphase_emp     =zeros(GridData.Lines_num,1);
    rms_err_Ireal_emp      =zeros(GridData.Lines_num,1);
    rms_err_Iimag_emp     =zeros(GridData.Lines_num,1);
    
    
    %standard error based on empirical test just concluded
    
    for n = 1 : GridData.Nodes_num
        
        std_err_Vmagnitude_emp(n,1) =     GridData.base_voltage*std(results_DSSE.err_Vmagnitude(:,n));
        std_err_Vphase_emp(n,1) =         std(results_DSSE.err_Vphase    (:,n));
        std_err_Vreal_emp(n,1) =          GridData.base_voltage*std(results_DSSE.err_Vreal(:,n));
        std_err_Vimag_emp(n,1) =          GridData.base_voltage*std(results_DSSE.err_Vimag(:,n));
        rms_err_Vmagnitude_emp(n,1) =     GridData.base_voltage*rms(results_DSSE.err_Vmagnitude(:,n));
        rms_err_Vphase_emp(n,1) =         rms(results_DSSE.err_Vphase    (:,n));
        rms_err_Vreal_emp(n,1) =          GridData.base_voltage*rms(results_DSSE.err_Vmagnitude(:,n));
        rms_err_Vimag_emp(n,1) =          rms(results_DSSE.err_Vphase    (:,n));
        mean_err_Vmagnitude_emp(n,1) =    GridData.base_voltage*mean(results_DSSE.err_Vmagnitude(:,n));
        mean_err_Vphase_emp(n,1) =        mean(results_DSSE.err_Vphase    (:,n));
    end
    for n = 1 : GridData.Lines_num
        std_err_Imagnitude_emp(n,1) =    GridData.base_current*std(results_DSSE.err_Imagnitude(:,n)); %GridData.base_current*
        std_err_Iphase_emp(n,1) =        std(results_DSSE.err_Iphase    (:,n));
        std_err_Ireal_emp(n,1) =         GridData.base_current*std(results_DSSE.err_Ireal(:,n)); %GridData.base_current*
        std_err_Iimag_emp(n,1) =         GridData.base_current*std(results_DSSE.err_Iimag(:,n));
        rms_err_Imagnitude_emp(n,1) =    GridData.base_current*rms(results_DSSE.err_Imagnitude(:,n)); %GridData.base_current*
        rms_err_Iphase_emp(n,1) =        rms(results_DSSE.err_Iphase    (:,n));
        rms_err_Ireal_emp(n,1) =         GridData.base_current*rms(results_DSSE.err_Ireal(:,n)); %GridData.base_current*
        rms_err_Iimag_emp(n,1) =  GridData.base_current*rms(results_DSSE.err_Iimag   (:,n));
        mean_err_Imagnitude_emp(n,1)=  GridData.base_current*mean(results_DSSE.err_Imagnitude(:,n));
        mean_err_Iphase_emp(n,1) =     mean(results_DSSE.err_Iphase    (:,n));
        mean_err_Ireal_emp(n,1)=  GridData.base_current*mean(results_DSSE.err_Ireal(:,n));
        mean_err_Iimag_emp(n,1) =     GridData.base_current*mean(results_DSSE.err_Iimag    (:,n));
    end
    
    
    %mean_error_last_two_magnitudes_DSSE = mean_err_Imagnitude_emp(end-1:end)
    if GridData.inj_status == 0
        [W,GridData] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
        [H_VRI] = Jacobian_m_VRIDSSE(GridData);
        [H_IRI] = Jacobian_m_IRIDSSE(GridData);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        W = full(W);H_VRI=full(H_VRI);H_IRI=full(H_IRI);
        % here we calculate the standard error based on mathematical evaluation
        Rx_rectangular_VRI=inv(H_VRI'*W*H_VRI); %this is the covariance matrix of the estimated state in rectangular format
        Rx_rectangular_IRI=inv(H_IRI'*W*H_IRI);
        var_err_rectangular_mat_VRI = diag(Rx_rectangular_VRI);
        std_err_rectangular_mat_VRI = sqrt(var_err_rectangular_mat_VRI);
        
        var_err_rectangular_mat_IRI = diag(Rx_rectangular_IRI);
        std_err_rectangular_mat_IRI = sqrt(var_err_rectangular_mat_IRI);
        
        [rotV,rm_column] = calc_rotV( GridData,PowerData,use_pu);
        [rotI,rm_column] = calc_rotI( GridData,PowerData,use_pu);
        %we apply the rotor
        Rx_polar_VRI = rotV*Rx_rectangular_VRI*rotV';
        Rx_polar_IRI = rotI*Rx_rectangular_IRI*rotI';
        var_err_polar_mat_IRI = diag(Rx_polar_IRI);
        var_err_polar_mat_VRI = diag(Rx_polar_VRI);
        std_err_polar_mat_IRI = sqrt(var_err_polar_mat_IRI);
        std_err_polar_mat_VRI = sqrt(var_err_polar_mat_VRI);
        % standard deviation of the error in polar format
        std_err_Vreal_mat(1) = GridData.base_voltage*std_err_rectangular_mat_VRI(1);
        if rm_column == 1
            std_err_Vimag_mat(1) = 0;
        else
            std_err_Vimag_mat(1) = GridData.base_voltage*std_err_rectangular_mat_VRI(2);
        end
        std_err_Vreal_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_rectangular_mat_VRI(2+(1-rm_column):2:2*GridData.Nodes_num-1+(1-rm_column));
        std_err_Vimag_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_rectangular_mat_VRI(3+(1-rm_column):2:2*GridData.Nodes_num-1+(1-rm_column));
        
        std_err_Vmagnitude_mat(1) = GridData.base_voltage*std_err_polar_mat_VRI(1);
        if rm_column == 1
            std_err_Vphase_mat(1) = 0;
        else
            std_err_Vphase_mat(1) = std_err_rectangular_mat_VRI(2);
        end
        
        std_err_Vmagnitude_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_polar_mat_VRI(2+(1-rm_column):2:2*GridData.Nodes_num-1+(1-rm_column));
        std_err_Vphase_mat(2:GridData.Nodes_num,1) = std_err_polar_mat_VRI(3+(1-rm_column):2:2*GridData.Nodes_num-1+(1-rm_column));
        
        std_err_Imagnitude_mat = GridData.base_current*std_err_polar_mat_IRI(2+(1-rm_column):2:2*GridData.Lines_num+1+(1-rm_column));
        std_err_Iphase_mat = std_err_polar_mat_IRI(3+(1-rm_column):2:2*GridData.Lines_num+1+(1-rm_column));
        std_err_Ireal_mat = GridData.base_current*std_err_rectangular_mat_IRI(2+(1-rm_column):2:2*GridData.Lines_num+1+(1-rm_column));
        std_err_Iimag_mat = GridData.base_current*std_err_rectangular_mat_IRI(3+(1-rm_column):2:2*GridData.Lines_num+1+(1-rm_column));
        std_err_Iri =  GridData.base_current*std_err_rectangular_mat_IRI;
        
    else
        ref = 11*GridData.DM.NGF+10*GridData.DM.NGS+2*GridData.DM.Nload;
        [W,GridData] = Weight_m(GridData,PowerData,Combination_devices,Accuracy);
        [H_IRI] = Jacobian_m_IRIDSSE(GridData);
        [H_VRI] = Jacobian_m_VRIDSSE(GridData);
        W = full(W);H_VRI=full(H_VRI);H_IRI=full(H_IRI);
        Rx_rectangular_IRI = inv(H_IRI'*W*H_IRI);
        Rx_rectangular_VRI = inv(H_VRI'*W*H_VRI);
        rotV = calc_rotV( GridData,PowerData,use_pu);
        rotI = calc_rotI( GridData,PowerData,use_pu);
        % here we remove the contribution of the slack bus voltage phase angle state (or
        % slack bus voltage imaginary state)
        var_err_rectangular_mat_VRI = diag(Rx_rectangular_VRI);
        std_err_rectangular_mat_VRI = sqrt(var_err_rectangular_mat_VRI);
        rotI = blkdiag(eye(ref),rotI);
        rotV = blkdiag(eye(ref),rotV);
        Rx_polar_IRI = rotI * Rx_rectangular_IRI * rotI';
        var_err_rect_mat_IRI  = diag(Rx_rectangular_IRI);
        
        var_err_polar_mat_IRI = diag(Rx_polar_IRI);
        std_err_polar_mat_IRI = sqrt(var_err_polar_mat_IRI);
        std_err_rectangular_mat_IRI = sqrt(var_err_rect_mat_IRI);
        
        Rx_polar_VRI = rotV*Rx_rectangular_VRI*rotV';
        var_err_polar_mat_VRI = diag(Rx_polar_VRI);
        std_err_polar_mat_VRI = sqrt(var_err_polar_mat_VRI);
        std_err_Vimag_mat(1) = 0;
        std_err_Vreal_mat(1) = GridData.base_voltage*std_err_rectangular_mat_VRI(ref+1);
        std_err_Vreal_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_rectangular_mat_VRI(ref+2:2:end);
        std_err_Vimag_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_rectangular_mat_VRI(ref+3:2:end);
        
        std_err_Vmagnitude_mat(1) = GridData.base_voltage*std_err_polar_mat_VRI(ref+1);
        std_err_Vphase_mat(1)     = 0;
        std_err_Vmagnitude_mat(2:GridData.Nodes_num,1) = GridData.base_voltage*std_err_polar_mat_VRI(ref+2:2:end);
        std_err_Vphase_mat    (2:GridData.Nodes_num,1) = std_err_polar_mat_VRI(ref+3:2:end);
        
        std_err_Imagnitude_mat = GridData.base_current*std_err_polar_mat_IRI(ref+2:2:end);
        std_err_Iphase_mat     =       std_err_polar_mat_IRI(ref+3:2:end);
        std_err_Ireal_mat      = GridData.base_current*std_err_rectangular_mat_IRI(ref+2:2:end);
        std_err_Iimag_mat      = GridData.base_current*std_err_rectangular_mat_IRI(ref+3:2:end);
        std_err_Iri =  GridData.base_current*std_err_rectangular_mat_IRI(ref+1:end);
        
        std_err_inj_status_mat = (GridData.base_status .* std_err_rectangular_mat_IRI(1:ref));
        std_err_inj_status_emp = (GridData.base_status' .* std(results_DSSE.err_inj_status,0,1))';
        rms_err_inj_status_emp = (GridData.base_status' .* std(results_DSSE.err_inj_status,1))';
        
        results_DSSE_Uncertainty.std_err_inj_status_emp = std_err_inj_status_emp;
        results_DSSE_Uncertainty.rms_err_inj_status_emp = rms_err_inj_status_emp;
        results_DSSE_Uncertainty.std_err_inj_status_mat = std_err_inj_status_mat;
        
    end
    
    for x = 1 : GridData.Lines_num
        Rx_rectangular_IRI_stdemp(2*(x-1)+1,2*(x-1)+1) = std_err_Ireal_emp(x,1)^2;
        Rx_rectangular_IRI_stdemp(2*(x-1)+2,2*(x-1)+2) = std_err_Iimag_emp(x,1)^2;
        Rx_rectangular_IRI_rmsemp(2*(x-1)+1,2*(x-1)+1) = rms_err_Ireal_emp(x,1)^2;
        Rx_rectangular_IRI_rmsemp(2*(x-1)+2,2*(x-1)+2) = rms_err_Iimag_emp(x,1)^2;
    end
    [rotI,rm_column] = calc_rotI( GridData,PowerData,0);
    Rx_polar_IRI_stdemp = rotI(2+(1-rm_column):end,2+(1-rm_column):end)*Rx_rectangular_IRI_stdemp*rotI(2+(1-rm_column):end,2+(1-rm_column):end)';
    Rx_polar_IRI_rmsemp = rotI(2+(1-rm_column):end,2+(1-rm_column):end)*Rx_rectangular_IRI_rmsemp*rotI(2+(1-rm_column):end,2+(1-rm_column):end)';
    for x = 1 : GridData.Lines_num
        std_err_Imagnitude_empmat(x,1)= sqrt(Rx_polar_IRI_stdemp(2*(x-1)+1,2*(x-1)+1));
        std_err_Iphase_empmat(x,1) = sqrt(Rx_polar_IRI_stdemp(2*(x-1)+2,2*(x-1)+2));
        rms_err_Imagnitude_empmat(x,1)= sqrt(Rx_polar_IRI_rmsemp(2*(x-1)+1,2*(x-1)+1));
        rms_err_Iphase_empmat(x,1) = sqrt(Rx_polar_IRI_rmsemp(2*(x-1)+2,2*(x-1)+2));
    end
    
    results_DSSE_Uncertainty.std_err_Iri = std_err_Iri;
    
    results_DSSE_Uncertainty.std_err_Vmagnitude_mat = std_err_Vmagnitude_mat;
    results_DSSE_Uncertainty.std_err_Vphase_mat = std_err_Vphase_mat;
    results_DSSE_Uncertainty.std_err_Vmagnitude_emp = std_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.std_err_Vphase_emp = std_err_Vphase_emp;
    results_DSSE_Uncertainty.rms_err_Vmagnitude_emp = rms_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.rms_err_Vphase_emp = rms_err_Vphase_emp;
    results_DSSE_Uncertainty.mean_err_Vmagnitude_emp = mean_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.mean_err_Vphase_emp = mean_err_Vphase_emp ;
    results_DSSE_Uncertainty.std_err_Vreal_emp = std_err_Vreal_emp;
    results_DSSE_Uncertainty.std_err_Vreal_mat = std_err_Vreal_mat;
    results_DSSE_Uncertainty.std_err_Vimag_emp = std_err_Vimag_emp;
    results_DSSE_Uncertainty.std_err_Vimag_mat = std_err_Vimag_mat;
    
    
    results_DSSE_Uncertainty.std_err_Imagnitude_mat = std_err_Imagnitude_mat;
    results_DSSE_Uncertainty.std_err_Iphase_mat = std_err_Iphase_mat;
    results_DSSE_Uncertainty.std_err_Imagnitude_emp = std_err_Imagnitude_emp;
    results_DSSE_Uncertainty.std_err_Iphase_emp = std_err_Iphase_emp;
    results_DSSE_Uncertainty.std_err_Imagnitude_empmat = std_err_Imagnitude_empmat;
    results_DSSE_Uncertainty.std_err_Iphase_empmat = std_err_Iphase_empmat;
    results_DSSE_Uncertainty.rms_err_Imagnitude_emp = rms_err_Imagnitude_emp;
    results_DSSE_Uncertainty.rms_err_Iphase_emp = rms_err_Iphase_emp;
    results_DSSE_Uncertainty.rms_err_Imagnitude_empmat = rms_err_Imagnitude_empmat;
    results_DSSE_Uncertainty.rms_err_Iphase_empmat = rms_err_Iphase_empmat;
    results_DSSE_Uncertainty.rms_err_Ireal_emp=rms_err_Ireal_emp;
    results_DSSE_Uncertainty.rms_err_Iimag_emp=rms_err_Iimag_emp;
    results_DSSE_Uncertainty.mean_err_Imagnitude_emp = mean_err_Imagnitude_emp ;
    results_DSSE_Uncertainty.mean_err_Iphase_emp = mean_err_Iphase_emp ;
    results_DSSE_Uncertainty.std_err_Ireal_emp = std_err_Ireal_emp;
    results_DSSE_Uncertainty.std_err_Ireal_mat = std_err_Ireal_mat;
    results_DSSE_Uncertainty.std_err_Iimag_emp = std_err_Iimag_emp;
    results_DSSE_Uncertainty.std_err_Iimag_mat = std_err_Iimag_mat;
    results_DSSE_Uncertainty.mean_err_Ireal_emp = mean_err_Ireal_emp ;
    results_DSSE_Uncertainty.mean_err_Iimag_emp = mean_err_Iimag_emp ;
    
    
    
    
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1 || strcmp(GridData.type_of_model,'three_phase_unbalance')==1
    
    std_err_Vmagnitude_emp =zeros(GridData.Nodes_num,3);
    std_err_Vphase_emp     =zeros(GridData.Nodes_num,3);
    std_err_Vreal_emp      =zeros(GridData.Nodes_num,3);
    std_err_Vimag_emp      =zeros(GridData.Nodes_num,3);
    mean_err_Vmagnitude_emp=zeros(GridData.Nodes_num,3);
    mean_err_Vphase_emp    =zeros(GridData.Nodes_num,3);
    rms_err_Vmagnitude_emp =zeros(GridData.Nodes_num,3);
    rms_err_Vphase_emp     =zeros(GridData.Nodes_num,3);
    rms_err_Vreal_emp      =zeros(GridData.Nodes_num,3);
    rms_err_Vimag_emp      =zeros(GridData.Nodes_num,3);
    std_err_Imagnitude_emp =zeros(GridData.Lines_num,3);
    std_err_Iphase_emp     =zeros(GridData.Lines_num,3);
    std_err_Ireal_emp      =zeros(GridData.Lines_num,3);
    std_err_Iimag_emp      =zeros(GridData.Lines_num,3);
    mean_err_Imagnitude_emp=zeros(GridData.Lines_num,3);
    mean_err_Iphase_emp    =zeros(GridData.Lines_num,3);
    mean_err_Ireal_emp     =zeros(GridData.Lines_num,3);
    mean_err_Iimag_emp     =zeros(GridData.Lines_num,3);
    rms_err_Imagnitude_emp =zeros(GridData.Lines_num,3);
    rms_err_Iphase_emp     =zeros(GridData.Lines_num,3);
    rms_err_Ireal_emp      =zeros(GridData.Lines_num,3);
    rms_err_Iimag_emp     =zeros(GridData.Lines_num,3);
    for n = 1 : GridData.Nodes_num
        for f = 1 : 3
            std_err_Vmagnitude_emp(n,f) =     GridData.base_voltage*std(results_DSSE.err_Vmagnitude([f:3:3*Test_SetUp.N_MC],n));
            std_err_Vphase_emp(n,f) =         std(results_DSSE.err_Vphase    ([f:3:3*Test_SetUp.N_MC],n));
            std_err_Vreal_emp(n,f) =          GridData.base_voltage*std(results_DSSE.err_Vreal([f:3:3*Test_SetUp.N_MC],n));
            std_err_Vimag_emp(n,f) =          GridData.base_voltage*std(results_DSSE.err_Vimag([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Vmagnitude_emp(n,f) =     GridData.base_voltage*rms(results_DSSE.err_Vmagnitude([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Vphase_emp(n,f) =         rms(results_DSSE.err_Vphase    ([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Vreal_emp(n,f) =          GridData.base_voltage*rms(results_DSSE.err_Vmagnitude([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Vimag_emp(n,f) =          rms(results_DSSE.err_Vphase    ([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Vmagnitude_emp(n,f) =    GridData.base_voltage*mean(results_DSSE.err_Vmagnitude([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Vphase_emp(n,f) =        mean(results_DSSE.err_Vphase    ([f:3:3*Test_SetUp.N_MC],n));
        end
    end
    for n = 1 : GridData.Lines_num
        for f = 1 : 3
            std_err_Imagnitude_emp(n,f) =    GridData.base_current*std(results_DSSE.err_Imagnitude([f:3:3*Test_SetUp.N_MC],n)); %GridData.base_current*
            std_err_Iphase_emp(n,f) =        std(results_DSSE.err_Iphase    ([f:3:3*Test_SetUp.N_MC],n));
            std_err_Ireal_emp(n,f) =         GridData.base_current*std(results_DSSE.err_Ireal([f:3:3*Test_SetUp.N_MC],n)); %GridData.base_current*
            std_err_Iimag_emp(n,f) =         GridData.base_current*std(results_DSSE.err_Iimag([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Imagnitude_emp(n,f) =    GridData.base_current*rms(results_DSSE.err_Imagnitude([f:3:3*Test_SetUp.N_MC],n)); %GridData.base_current*
            rms_err_Iphase_emp(n,f) =        rms(results_DSSE.err_Iphase    ([f:3:3*Test_SetUp.N_MC],n));
            rms_err_Ireal_emp(n,f) =         GridData.base_current*rms(results_DSSE.err_Ireal([f:3:3*Test_SetUp.N_MC],n)); %GridData.base_current*
            rms_err_Iimag_emp(n,f) =  GridData.base_current*rms(results_DSSE.err_Iimag   ([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Imagnitude_emp(n,f)=  GridData.base_current*mean(results_DSSE.err_Imagnitude([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Iphase_emp(n,f) =     mean(results_DSSE.err_Iphase    ([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Ireal_emp(n,f)=  GridData.base_current*mean(results_DSSE.err_Ireal([f:3:3*Test_SetUp.N_MC],n));
            mean_err_Iimag_emp(n,f) =     GridData.base_current*mean(results_DSSE.err_Iimag    ([f:3:3*Test_SetUp.N_MC],n));
        end
    end
    results_DSSE_Uncertainty.std_err_Vmagnitude_emp =std_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.std_err_Vphase_emp     =std_err_Vphase_emp;
    results_DSSE_Uncertainty.std_err_Vreal_emp      =std_err_Vreal_emp;
    results_DSSE_Uncertainty.std_err_Vimag_emp      =std_err_Vimag_emp;
    results_DSSE_Uncertainty.mean_err_Vmagnitude_emp=mean_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.mean_err_Vphase_emp    =mean_err_Vphase_emp;
    results_DSSE_Uncertainty.rms_err_Vmagnitude_emp =rms_err_Vmagnitude_emp;
    results_DSSE_Uncertainty.rms_err_Vphase_emp     =rms_err_Vphase_emp;
    results_DSSE_Uncertainty.rms_err_Vreal_emp      =rms_err_Vreal_emp;
    results_DSSE_Uncertainty.rms_err_Vimag_emp      =rms_err_Vimag_emp;
    results_DSSE_Uncertainty.std_err_Imagnitude_emp =std_err_Imagnitude_emp;
    results_DSSE_Uncertainty.std_err_Iphase_emp     =std_err_Iphase_emp;
    results_DSSE_Uncertainty.std_err_Ireal_emp      =std_err_Ireal_emp;
    results_DSSE_Uncertainty.std_err_Iimag_emp      =std_err_Iimag_emp;
    results_DSSE_Uncertainty.mean_err_Imagnitude_emp=mean_err_Imagnitude_emp;
    results_DSSE_Uncertainty.mean_err_Iphase_emp    =mean_err_Iphase_emp;
    results_DSSE_Uncertainty.mean_err_Ireal_emp     =mean_err_Ireal_emp;
    results_DSSE_Uncertainty.mean_err_Iimag_emp     =mean_err_Iimag_emp;
    results_DSSE_Uncertainty.rms_err_Imagnitude_emp =rms_err_Imagnitude_emp;
    results_DSSE_Uncertainty.rms_err_Iphase_emp     =rms_err_Iphase_emp;
    results_DSSE_Uncertainty.rms_err_Ireal_emp      =rms_err_Ireal_emp;
    results_DSSE_Uncertainty.rms_err_Iimag_emp      =rms_err_Iimag_emp;
    
end

end