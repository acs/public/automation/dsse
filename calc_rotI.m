%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate matrix to convert rectangular currents into polar currents
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [rotI,rm_column] = calc_rotI( GridData,PowerData,use_pu)
% R = GridData.R2; %this are actually the inverses of the resistances
% X = GridData.X2; %this are actually the inverses of the reactances
if isfield(GridData,'rm_column') == 1
    rm_column = GridData.rm_column;
else
    rm_column = 1;
end
if use_pu == 0
    PowerDataVmagn = PowerData.Vmagn * GridData.base_voltage;
    PowerDataImagn = PowerData.Imagn* GridData.base_current;
else
    PowerDataVmagn = PowerData.Vmagn ;
    PowerDataImagn = PowerData.Imagn ;
end

if rm_column == 1
rotI = zeros(1 + 2*GridData.Lines_num, 1 + 2*GridData.Lines_num);
rotI(1,1) = cos(PowerData.Vph(1,1));  
else
rotI = zeros(2 + 2*GridData.Lines_num, 2 + 2*GridData.Lines_num);
rotI(1,1) = cos(PowerData.Vph(1,1));
rotI(1,2) = sin(PowerData.Vph(1,1));
rotI(2,1) = - sin(PowerData.Vph(1,1))/PowerDataVmagn(1,1);
rotI(2,2) = cos(PowerData.Vph(1,1))/PowerDataVmagn(1,1);     
end


for x = 1 :  GridData.Lines_num
    if rm_column == 1

        rotI(2*(x-1)+1+2-1,2*(x-1)+1+2-1) = cos(PowerData.Iph(x,1));
        rotI(2*(x-1)+1+2-1,2*(x-1)+2+2-1) = sin(PowerData.Iph(x,1));
        rotI(2*(x-1)+2+2-1,2*(x-1)+1+2-1) = - sin(PowerData.Iph(x,1))/PowerDataImagn(x,1);
        rotI(2*(x-1)+2+2-1,2*(x-1)+2+2-1) = cos(PowerData.Iph(x,1))/PowerDataImagn(x,1);
    else

        rotI(2*(x-1)+1+2,2*(x-1)+1+2) = cos(PowerData.Iph(x,1));
        rotI(2*(x-1)+1+2,2*(x-1)+2+2) = sin(PowerData.Iph(x,1));
        rotI(2*(x-1)+2+2,2*(x-1)+1+2) = - sin(PowerData.Iph(x,1))/PowerDataImagn(x,1);
        rotI(2*(x-1)+2+2,2*(x-1)+2+2) = cos(PowerData.Iph(x,1))/PowerDataImagn(x,1);
    end
end
end

