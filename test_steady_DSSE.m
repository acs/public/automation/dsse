%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Script to test the state estimator on power system, single phase and
% three phase models.
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
% structs where the results of the state estimates are stored
results_DSSE_WLS2=struct;results_DSSE_WLS2.err_Vmagnitude=[];results_DSSE_WLS2.err_Vphase=[];results_DSSE_WLS2.Vmagn_status=[];results_DSSE_WLS2.Vph_status=[];results_DSSE_WLS2.Imagn_status=[];results_DSSE_WLS2.Iph_status=[];results_DSSE_WLS2.Vreal_status=[];results_DSSE_WLS2.Vimag_status=[];results_DSSE_WLS2.Ireal_status=[];results_DSSE_WLS2.Iimag_status=[];results_DSSE_WLS2.Vmagn_true=[];results_DSSE_WLS2.Vph_true=[];results_DSSE_WLS2.Imagn_true=[];results_DSSE_WLS2.Iph_true=[];results_DSSE_WLS2.Vreal_true=[];results_DSSE_WLS2.Vimag_true=[];results_DSSE_WLS2.Ireal_true=[];results_DSSE_WLS2.Iimag_true=[];results_DSSE_WLS2.err_Imagnitude=[];results_DSSE_WLS2.err_Iphase=[];results_DSSE_WLS2.err_Ireal=[];results_DSSE_WLS2.err_Iimag=[];results_DSSE_WLS2.err_Vreal=[];results_DSSE_WLS2.err_Vimag=[];results_DSSE_WLS2.err_Vmagnitude_in   =[];results_DSSE_WLS2.err_Vphase_in=[];results_DSSE_WLS2.err_Imagnitude_in  =[];results_DSSE_WLS2.err_Iphase_in   =[];results_DSSE_WLS2.err_Ireal_in   =[];results_DSSE_WLS2.err_Iimag_in =[];results_DSSE_WLS2.Vmagn_measured =[];results_DSSE_WLS2.Vph_measured =[];
results_DSSE_WLS=struct;results_DSSE_WLS.err_Vmagnitude=[];results_DSSE_WLS.err_Vphase=[];results_DSSE_WLS.Vmagn_status=[];results_DSSE_WLS.Vph_status=[];results_DSSE_WLS.Imagn_status=[];results_DSSE_WLS.Iph_status=[];results_DSSE_WLS.Vreal_status=[];results_DSSE_WLS.Vimag_status=[];results_DSSE_WLS.Ireal_status=[];results_DSSE_WLS.Iimag_status=[];results_DSSE_WLS.Vmagn_true=[];results_DSSE_WLS.Vph_true=[];results_DSSE_WLS.Imagn_true=[];results_DSSE_WLS.Iph_true=[];results_DSSE_WLS.Vreal_true=[];results_DSSE_WLS.Vimag_true=[];results_DSSE_WLS.Ireal_true=[];results_DSSE_WLS.Iimag_true=[];results_DSSE_WLS.err_Imagnitude=[];results_DSSE_WLS.err_Iphase=[];results_DSSE_WLS.err_Ireal=[];results_DSSE_WLS.err_Iimag=[];results_DSSE_WLS.err_Vreal=[];results_DSSE_WLS.err_Vimag=[];results_DSSE_WLS.err_Vmagnitude_in   =[];results_DSSE_WLS.err_Vphase_in=[];results_DSSE_WLS.err_Imagnitude_in  =[];results_DSSE_WLS.err_Iphase_in   =[];results_DSSE_WLS.err_Ireal_in   =[];results_DSSE_WLS.err_Iimag_in =[];results_DSSE_WLS.Vmagn_measured =[];results_DSSE_WLS.Vph_measured =[];
%select type of model among 'single_phase' 'three_phase_sequence' 'three_phase_unbalance' 
type_of_model = 'single_phase';
name_of_model = 'CIGRE_MV';
[GridData] = generate_GridData(type_of_model,name_of_model);%in this function the static model is generated
[PowerData] = generate_PowerData(GridData,name_of_model); %in this function the power flow is run in order to obtain the reference values to test the state estimator
[Test_SetUp,Combination_devices,Accuracy] = generate_DSSEConfData(GridData);%here the test configuration data are set: measurement devices location and accuracy
GridData.rm_column = 0;%in this case the phase angle at 1st bus is also considered as state
[W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy); %weight and covariance matrix of the state estimator
[Meas_true_vector] = calc_Mvector_external(GridData,PowerData); %vector with reference values of the measurements

for z = 1 : Test_SetUp.N_MC %in this for loop the Monte Carlo tests are run
    %Number_MC = z

    Meas_vector_external = mvnrnd(Meas_true_vector, R)'; %the measurements are corrupted following the covariance matrix
    [Vmagn_status_WLS,Vph_status_WLS]   =  VRIDSSE(Meas_vector_external,W,GridData,Test_SetUp); %voltage state estimator
    
    [Vmagn_status_WLS2,Vph_status_WLS2,Imagn_status_WLS2,Iph_status_WLS2] = IRIDSSE(Meas_vector_external,W,GridData,Test_SetUp);%current state estimator
    
    [results_DSSE_WLS]              = Data_Output(Vmagn_status_WLS,Vph_status_WLS,results_DSSE_WLS,GridData,PowerData); %collects the metrics of the error
    [results_DSSE_WLS2]             = Data_Output(Vmagn_status_WLS2,Vph_status_WLS2,results_DSSE_WLS2,GridData,PowerData);

end

[results_DSSE_Uncertainty_WLS]  = calc_Unc_DSSE(results_DSSE_WLS,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp); %calculate uncertainty of the estimator
[results_DSSE_Uncertainty_WLS2] = calc_Unc_DSSE(results_DSSE_WLS2,PowerData,GridData,Combination_devices,Accuracy,Test_SetUp);

