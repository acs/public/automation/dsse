%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function to calculate the weight matrix and the covariance matrices
%
% @author Andrea Angioni <aangioni@eonerc.rwth-aachen.de>
% @copyright 2018, Institute for Automation of Complex Power Systems, EONERC
% @license GNU General Public License (version 3)
%
% dsse
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [W,GridData,R] = Weight_m(GridData,PowerData,Combination_devices,Accuracy)

LM = 1e-12; %minimum acceptable variance
Gx = 0;
if Combination_devices.Vph_measure(1,1) == 1
    Gx = Gx + 1;
end

if strcmp(GridData.type_of_model,'single_phase') == 1

    n = 0;
    LocationMeas = 0;
    TypeMeas = 0;
    W = 0;
    R = 0;
    
    % we search for the available measurements in all the nodes and later in all the branches
    if isfield(Combination_devices,'xstatus_measure') == 1
        if GridData.inj_status == 1 %in case we consider dynamic measuremetns
            for x = 1 : 11*GridData.DM.NGF + 10*GridData.DM.NGS + 2*GridData.DM.Nload
                if Combination_devices.xstatus_measure(1,x)==1
                    Rtemp = (Accuracy.Accuracy_pseudo*PowerData.x_status(x)).^2;
                    if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n +1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 100;
                    DelayMeas(n,1) = Combination_devices.xstatus_measure(2,x);
                end
            end
        end
    end
    
    for x=1:GridData.Nodes_num
        if Combination_devices.P_measure(1,x)==1 && Combination_devices.Q_measure(1,x)==1
            rotPQ(1,1) = cos(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
            rotPQ(1,2) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
            rotPQ(2,1) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
            rotPQ(2,2) =-cos(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
            Rtemp = rotPQ*[(Accuracy.Accuracy_P*PowerData.Pinj(x))^2, 0; 0 (Accuracy.Accuracy_Q*PowerData.Qinj(x))^2]*rotPQ';
            if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
            if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
            R(n+1:n+2,n+1:n+2) = Rtemp;
            W(n+1:n+2,n+1:n+2)= Rtemp^-1;
            n=n+1;
            LocationMeas(n,1) = x;
            TypeMeas(n,1) = 1;% 4 represent measurements of type voltage magnitude
            DelayMeas(n,1) = Combination_devices.P_measure(2,x);
            n=n+1;
            LocationMeas(n,1) = x;
            TypeMeas(n,1) = 2;% 4 represent measurements of type voltage phase angle
            DelayMeas(n,1) = Combination_devices.Q_measure(2,x);
            
        elseif Combination_devices.Pseudo_measure(1,x) == 1
%             rotPQ(1,1) = cos(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
%             rotPQ(1,2) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
%             rotPQ(2,1) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
%             rotPQ(2,2) =-cos(PowerData.Vph(x))/(PowerData.Vmagn(x)^2);
            
            rotPQ(1,1) = cos(PowerData.Vph(x))/(PowerData.Vmagn(x));
            rotPQ(1,2) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x));
            rotPQ(2,1) = sin(PowerData.Vph(x))/(PowerData.Vmagn(x));
            rotPQ(2,2) =-cos(PowerData.Vph(x))/(PowerData.Vmagn(x));
            Rtemp = rotPQ*[(Accuracy.Accuracy_pseudo*PowerData.Pinj(x))^2, 0; 0 (Accuracy.Accuracy_pseudo*PowerData.Qinj(x))^2]*rotPQ';

            if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
            if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
            R(n+1:n+2,n+1:n+2) = Rtemp;
            W(n+1:n+2,n+1:n+2)= Rtemp^-1;
            n=n+1;
            LocationMeas(n,1) = x;
            TypeMeas(n,1) = 1;% 4 represent measurements of type voltage magnitude
            DelayMeas(n,1) = Combination_devices.Pseudo_measure(2,x);
            n=n+1;
            LocationMeas(n,1) = x;
            TypeMeas(n,1) = 2;% 4 represent measurements of type voltage phase angle
            DelayMeas(n,1) = Combination_devices.Pseudo_measure(2,x);
        end
        
        % voltage real part - here it is converted from the covariance of magnitude and phase angle
        if Combination_devices.Vmagn_measure(1,x)==1 && Combination_devices.Vph_measure(1,x)==1
            if x == 1  % we remove the phase measurement from the first bus
                
                if isfield(GridData,'rm_column') == 1
                    if GridData.rm_column == 0
                        rotV(1,1) = cos(PowerData.Vph(x));
                        rotV(1,2) =  - sin(PowerData.Vph(x))*PowerData.Vmagn(x) ;
                        rotV(2,1) = sin(PowerData.Vph(x)) ;
                        rotV(2,2) =   cos(PowerData.Vph(x))*PowerData.Vmagn(x);
                        Rtemp = rotV*[(Accuracy.Accuracy_Vmagn*PowerData.Vmagn(x))^2, 0; 0 (Accuracy.Accuracy_Vph)^2]*rotV';
                        if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                        if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
                        R(n+1:n+2,n+1:n+2) = Rtemp;
                        W(n+1:n+2,n+1:n+2)= Rtemp^-1;
                        n=n+1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 3;% 4 represent measurements of type voltage magnitude
                        DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                        n=n+1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 4;% 4 represent measurements of type voltage phase angle
                        DelayMeas(n,1) = Combination_devices.Vph_measure(2,x);
                    else
                        rotV(1,1) = cos(PowerData.Vph(x));
                        Rtemp = rotV*(Accuracy.Accuracy_Vmagn*PowerData.Vmagn(x))^2*rotV';
                        if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                        R(n+1,n+1) = Rtemp;
                        W(n+1,n+1) = Rtemp^-1;
                        n=n+1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 3;% 3 represent measurements of type voltage magnitude
                        DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                    end
                else
                    rotV(1,1) = cos(PowerData.Vph(x));
                    Rtemp = rotV*(Accuracy.Accuracy_Vmagn*PowerData.Vmagn(x))^2*rotV';
                    if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n=n+1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 3;% 3 represent measurements of type voltage magnitude
                    DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                end
                

            else

                rotV(1,1) = cos(PowerData.Vph(x));
                rotV(1,2) =  - sin(PowerData.Vph(x))*PowerData.Vmagn(x) ;
                rotV(2,1) = sin(PowerData.Vph(x)) ;
                rotV(2,2) =   cos(PowerData.Vph(x))*PowerData.Vmagn(x);
                Rtemp = rotV*[(Accuracy.Accuracy_Vmagn*PowerData.Vmagn(x))^2, 0; 0 (Accuracy.Accuracy_Vph)^2]*rotV';
                if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
                R(n+1:n+2,n+1:n+2) = Rtemp;
                W(n+1:n+2,n+1:n+2)= Rtemp^-1;
                n=n+1;
                LocationMeas(n,1) = x;
                TypeMeas(n,1) = 3;% 4 represent measurements of type voltage magnitude
                DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                n=n+1;
                LocationMeas(n,1) = x;
                TypeMeas(n,1) = 4;% 4 represent measurements of type voltage phase angle
                DelayMeas(n,1) = Combination_devices.Vph_measure(2,x);
            end
        end
    end
    
    % now we search for measurements in the lines
    for b = 1 : GridData.Lines_num
        % Current real part - already converted from magnitude and phase angle
        if Combination_devices.Imagn_measure(1,b)~=0 && Combination_devices.Iph_measure(1,b)~=0

            rotI(1,1) = cos(PowerData.Iph(b,1));
            rotI(1,2) =  - sin(PowerData.Iph(b,1))*PowerData.Imagn(b,1) ;
            rotI(2,1) = sin(PowerData.Iph(b,1)) ;
            rotI(2,2) =   cos(PowerData.Iph(b,1))*PowerData.Imagn(b,1);
            std_dev = [(Accuracy.Accuracy_Imagn*PowerData.Imagn(b))^2, 0; 0 (Accuracy.Accuracy_Iph)^2];
            Rtemp = rotI * std_dev *(rotI');
            if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
            if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
            R(n+1:n+2,n+1:n+2) = Rtemp;
            W(n+1:n+2,n+1:n+2)= Rtemp^-1;
            n=n+1;
            LocationMeas(n,1) = b;%in the 1st column we assign the line
            TypeMeas(n,1) = 5;% 5 represent measurements of type current magnitude
            DelayMeas(n,1) = Combination_devices.Imagn_measure(2,b);
            n=n+1;
            LocationMeas(n,1)=b;%in the 1st column we assign the line
            TypeMeas(n,1)=6;% 6 represent measurements of type current ph angle
            DelayMeas(n,1) = Combination_devices.Iph_measure(2,b);
        end
        
        if Combination_devices.Pflow_measure(1,b)~=0 %active power flow measurement
            Rtemp = (Accuracy.Accuracy_Pflow * PowerData.Pflow(b))^2;
            if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
            R(n+1,n+1) = Rtemp;
            W(n+1,n+1) = Rtemp^-1;
            n=n+1;
            LocationMeas(n,1) = b;%in the 1st column we assign the line
            TypeMeas(n,1) = 7; % 7 represent measurements of type power active flow
            DelayMeas(n,1) = Combination_devices.Pflow_measure(2,b);
        end
        
        if Combination_devices.Qflow_measure(1,b)~=0 %reactive power flow measurement
            Rtemp = (Accuracy.Accuracy_Qflow * PowerData.Qflow(b))^2;
            if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
            R(n+1,n+1) = Rtemp;
            W(n+1,n+1) = Rtemp^-1;
            n=n+1;
            LocationMeas(n,1) = b;%in the 1st column we assign the line
            TypeMeas(n,1) = 8; % 8 represent measurements of type power reactive flow
            DelayMeas(n,1) = Combination_devices.Qflow_measure(2,b);
        end
    end
    
elseif strcmp(GridData.type_of_model,'three_phase_sequence')==1  || strcmp(GridData.type_of_model,'three_phase_unbalance')==1

    n = 0;
    LocationMeas = 0;
    TypeMeas = 0;
    W = 0; R = 0;
    
    for x = 1 : GridData.Nodes_num
        for f = 1 : 3
            if GridData.present_node(f,x) ~= 0
                if Combination_devices.P_measure(1,x)==1 %active power measurement
                    Rtemp = (Accuracy.Accuracy_P*PowerData.Pinj(f,x))^2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 1;% 1 represent measurements of type active power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.P_measure(2,x);
                    
                elseif Combination_devices.Pseudo_measure(1,x) == 1 %pseudo measurements, but we do not consider pseudo measurements at the slack bus
                    Rtemp = (Accuracy.Accuracy_pseudo*PowerData.Pinj(f,x))^2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 1;% 1 represent measurements of type active power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Pseudo_measure(2,x);
                end
                
                if Combination_devices.Q_measure(1,x)==1%reactive power measurement
                    Rtemp = (Accuracy.Accuracy_Q*PowerData.Qinj(f,x))^+2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 2;% 2 represent measurements of type reactive power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Q_measure(2,x);
                    
                elseif Combination_devices.Pseudo_measure(1,x) == 1 %pseudo measurements, but we do not consider pseudo measurements at the slack bus
                    Rtemp = (Accuracy.Accuracy_pseudo*PowerData.Qinj(f,x))^2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = x;
                    TypeMeas(n,1) = 2;% 2 represent measurements of type reactive power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Pseudo_measure(2,x);
                end
                
                % voltage real part - here it is converted from the covariance of magnitude and phase angle
                if Combination_devices.Vmagn_measure(1,x)==1 && Combination_devices.Vph_measure(1,x)==1
                    if x == 1 % we remove the phase measurement from the first bus
                        rotV(1,1) = cos(PowerData.Vph(f,x));
                        Rtemp = rotV*((Accuracy.Accuracy_Vmagn*PowerData.Vmagn(f,x))^2)*rotV';
                        if Rtemp < LM; Rtemp = LM; end
                        R(n+1,n+1) = Rtemp;
                        W(n+1,n+1) = Rtemp^-1;
                        n = n + 1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 3;% 3 represent measurements of type voltage magnitude
                        PhaseMeas(n,1) = f;
                        DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                    else
                        
                        rotV(1,1) = cos(PowerData.Vph(f,x));
                        rotV(1,2) =  - sin(PowerData.Vph(f,x))*PowerData.Vmagn(f,x) ;
                        rotV(2,1) = sin(PowerData.Vph(f,x)) ;
                        rotV(2,2) =   cos(PowerData.Vph(f,x))*PowerData.Vmagn(f,x);
                        
                        Rtemp = rotV*[(Accuracy.Accuracy_Vmagn*PowerData.Vmagn(f,x))^2, 0; 0 (Accuracy.Accuracy_Vph)^2]*rotV';
                        if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                        if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
                        R(n+1:n+2,n+1:n+2) = Rtemp;
                        W(n+1:n+2,n+1:n+2)= Rtemp^-1;
                        
                        n=n+1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 3;% 4 represent measurements of type voltage magnitude
                        PhaseMeas(n,1) = f;
                        DelayMeas(n,1) = Combination_devices.Vmagn_measure(2,x);
                        n=n+1;
                        LocationMeas(n,1) = x;
                        TypeMeas(n,1) = 4;% 4 represent measurements of type voltage phase angle
                        PhaseMeas(n,1) = f;
                        DelayMeas(n,1) = Combination_devices.Vph_measure(2,x);
                    end
                end
            end
        end
    end
    % now we search for measurements in the lines
    for b = 1 : GridData.Lines_num
        for f = 1 : 3
            if GridData.present_line(f,b) ~= 0
                % Current real part - already converted from magnitude and phase angle
                if Combination_devices.Imagn_measure(1,b)~=0 && Combination_devices.Iph_measure(1,b)~=0
                    rotI(1,1) = cos(PowerData.Iph(f,b));
                    rotI(1,2) =  - sin(PowerData.Iph(f,b))*PowerData.Imagn(f,b) ;
                    rotI(2,1) = sin(PowerData.Iph(f,b)) ;
                    rotI(2,2) =   cos(PowerData.Iph(f,b))*PowerData.Imagn(f,b);
                    std_dev = [(Accuracy.Accuracy_Imagn*PowerData.Imagn(b))^2, 0; 0 (Accuracy.Accuracy_Iph)^2];
                    Rtemp = rotI * std_dev *(rotI');
                    if Rtemp(1,1) < LM; Rtemp(1,1) = LM; end
                    if Rtemp(2,2) < LM; Rtemp(2,2) = LM; end
                    R(n+1:n+2,n+1:n+2) = Rtemp;
                    W(n+1:n+2,n+1:n+2)= Rtemp^-1;
                    n=n+1;
                    LocationMeas(n,1) = b;
                    TypeMeas(n,1) = 5;% 5 represent measurements of type current magnitude
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Imagn_measure(2,b);
                    n=n+1;
                    LocationMeas(n,1)=b;
                    TypeMeas(n,1)=6;% 6 represent measurements of type current ph angle
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Iph_measure(2,b);
                end
                if Combination_devices.Pflow_measure(1,b)~=0
                    Rtemp = (Accuracy.Accuracy_Pflow*PowerData.Pflow(f,b))^2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = b;
                    TypeMeas(n,1) = 7;% 1 represent measurements of type active power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Pflow_measure(2,b);
                end
                
                if Combination_devices.Qflow_measure(1,b)~=0
                    Rtemp = (Accuracy.Accuracy_Qflow*PowerData.Qflow(f,b))^2;
                    if Rtemp < LM; Rtemp = LM; end
                    R(n+1,n+1) = Rtemp;
                    W(n+1,n+1) = Rtemp^-1;
                    n = n + 1;
                    LocationMeas(n,1) = b;
                    TypeMeas(n,1) = 8;% 1 represent measurements of type active power
                    PhaseMeas(n,1) = f;
                    DelayMeas(n,1) = Combination_devices.Qflow_measure(2,b);
                end
            end
        end
    end
    GridData.PhaseMeas = PhaseMeas;
end
GridData.MeasNum = n;
GridData.TypeMeas = TypeMeas;
GridData.LocationMeas = LocationMeas;
GridData.DelayMeas = DelayMeas;
end